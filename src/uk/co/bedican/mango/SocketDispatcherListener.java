/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.net.*;

/**
 * A listner interface for receiving socket dispatcher events.
 */
public interface SocketDispatcherListener
{
	/**
	 * Invoked when a socket is accepted.
	 *
	 * @param socket The socket which has been accepted.
	 */
	public void socketAccepted(Socket socket);
}
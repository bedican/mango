/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

/**
 * Signals an error has occured in while handling the request.
 */
public class RequestHandlerException extends Exception
{
	/**
	 * Constructs a new RequestHandlerException with the specified cause.
	 *
	 * @param cause The cause.
	 */
	public RequestHandlerException(Throwable cause) { super(cause); }

	/**
	 * Constructs a new RequestHandlerException with the specified detail message and cause.
	 *
	 * @param message The detail message.
	 * @param cause The cause.
	 */
	public RequestHandlerException(String message, Throwable cause) { super(message, cause); }

	/**
	 * Constructs a new RequestHandlerException with the specified detail message.
	 *
	 * @param message The detail message.
	 */
	public RequestHandlerException(String message) { super(message); }
}
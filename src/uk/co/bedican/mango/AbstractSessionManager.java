/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.util.*;

/**
 * Defines common functionality for a SessionManager.
 */
public abstract class AbstractSessionManager implements SessionManager
{
	private List<SessionListener> listeners = new Vector<SessionListener>();

	/**
	 * Retrieves a session from the store from a given session ID.
	 * If the session does not exist and the create parameter is true, a new session will be returned, otherwise null.
	 * This method should be implemented in concrete subclasses as the logic of retrieving a session object.
	 *
	 * @param sessionId The ID of the session to retrieve.
	 * @param create Whether to create a new session if the session for the given ID does not exist.
	 * @return A session for the given ID or if no session is available a new session if the create parameter is true otherwise null.
	 * @throws SessionManagerException If there was a problem with creation or retrieval of the session.
	 * @see #getSession(String, boolean)
	 */
	protected abstract Session load(String sessionId, boolean create) throws SessionManagerException;

	/**
	 * Saves a given session to the store.
	 * This method should be implemented in concrete subclasses as the logic of saving a session object.
	 *
	 * @param session The session to save.
	 * @throws SessionManagerException If there was a problem with saving the session.
	 * @see #putSession(Session)
	 */
	protected abstract void save(Session session) throws SessionManagerException;

	/**
	 * Removes a given session from the store.
	 * This method should be implemented in concrete subclasses as the logic of removing a session object.
	 *
	 * @param session The session to remove.
	 * @throws SessionManagerException If there was a problem with removing the session.
	 * @see #removeSession(Session)
	 */
	protected abstract void delete(Session session) throws SessionManagerException;

	/**
	 * Returns a new unique session ID, providing a default mechanism for generating an ID.
	 * This method can be optionally overriden to implement an alternative mechanism for unique ID generation.
	 *
	 * @return A unique session ID.
	 * @see #generateId()
	 */
	protected String newId()
	{
		return UUID.randomUUID().toString();
	}

	/**
	 * Returns a new unique session ID.
	 *
	 * @return A unique session ID.
	 * @see #newId()
	 */
	public final String generateId()
	{
		synchronized(this) {
			return this.newId();
		}
	}

	/**
	 * Retrieves a session from the store from a given session ID.
	 * If the session does not exist and the create parameter is true, a new session will be returned, otherwise null.
	 *
	 * @param sessionId The ID of the session to retrieve.
	 * @param create Whether to create a new session if the session for the given ID does not exist.
	 * @return A session for the given ID or if no session is available a new session if the create parameter is true otherwise null.
	 * @throws SessionManagerException If there was a problem with creation or retrieval of the session.
	 * @throws NullPointerException If sessionId is null.
	 * @throws IllegalArgumentException If sessionId is an empty string.
	 * @see #load(String, boolean)
	 */
	public final Session getSession(String sessionId, boolean create) throws SessionManagerException
	{
		if(sessionId == null) {
			throw new NullPointerException();
		}
		if(sessionId.isEmpty()) {
			throw new IllegalArgumentException("Missing sessionId");
		}

		Session session = this.load(sessionId, create);

		if((session != null) && (session.isNew())) {
			this.fireSessionCreated(session);
		}

		return session;
	}

	/**
	 * Saves a given session to the store.
	 *
	 * @param session The session to save.
	 * @throws SessionManagerException If there was a problem with saving the session.
	 * @throws NullPointerException If session is null.
	 * @see #save(Session)
	 */
	public final void putSession(Session session) throws SessionManagerException
	{
		if(session == null) {
			throw new NullPointerException();
		}
		if(!session.isValid()) {
			throw new SessionManagerException("Session is no longer valid for storage");
		}

		this.save(session);
	}

	/**
	 * Removes a given session from the store.
	 *
	 * @param session The session to remove.
	 * @throws SessionManagerException If there was a problem with removing the session.
	 * @throws NullPointerException If session is null.
	 * @see #delete(Session)
	 */
	public final void removeSession(Session session) throws SessionManagerException
	{
		if(session == null) {
			throw new NullPointerException();
		}

		this.delete(session);
		this.fireSessionDestroyed(session);
	}

	/**
	 * Adds a session listener to this session manager to listen for session events.
	 *
	 * @param listener The session listener.
	 * @throws NullPointerException If listener is null.
	 */
	public void addListener(SessionListener listener)
	{
		if(listener == null) {
			throw new NullPointerException();
		}
		if(this.listeners.contains(listener)) {
			return;
		}

		this.listeners.add(listener);
	}

	/**
	 * Removes a session listener from this session manager.
	 *
	 * @param listener The session listener.
	 * @throws NullPointerException If listener is null.
	 */
	public void removeListener(SessionListener listener)
	{
		if(listener == null) {
			throw new NullPointerException();
		}

		this.listeners.remove(listener);
	}

	/**
	 * Called on server shutdown.
	 * This class provides an empty implementation designed to be overriden by concrete subclasses.
	 */
	public void shutdown()
	{
	}

	private void fireSessionCreated(Session session)
	{
		if(session == null) {
			throw new NullPointerException();
		}

		SessionListener[] listeners = this.listeners.toArray(new SessionListener[0]);
		for(SessionListener listener:listeners) {
			listener.sessionCreated(session);
		}
	}
	private void fireSessionDestroyed(Session session)
	{
		if(session == null) {
			throw new NullPointerException();
		}

		SessionListener[] listeners = this.listeners.toArray(new SessionListener[0]);
		for(SessionListener listener:listeners) {
			listener.sessionDestroyed(session);
		}
	}
}
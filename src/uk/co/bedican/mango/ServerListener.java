/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

/**
 * <p>A listner interface for receiving server events.</p>
 * <p>The class that is interested in processing a server event either implements this interface (and all the methods it contains) or extends the abstract ServerAdapter class (overriding only the methods of interest).</p>
 *
 * @see ServerAdapter
 */
public interface ServerListener
{
	/**
	 * Invoked when a Server is started.
	 *
	 * @param server The server which has been started.
	 */
	public void serverStarted(Server server);

	/**
	 * Invoked when a Server has been requested to stop.
	 *
	 * @param server The server which has been requested to stop.
	 */
	public void serverStopping(Server server);

	/**
	 * Invoked when a Server is stopped.
	 *
	 * @param server The server which has been stopped.
	 */
	public void serverStopped(Server server);
}
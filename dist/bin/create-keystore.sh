#!/bin/bash

DIR=`dirname $0`
cd $DIR/../ssl

if [ -f mango.jks ]; then
	echo A keystore already exists.
else
	keytool -genkey -alias mango -keyalg RSA -keysize 2048 -keystore mango.jks
fi

/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.util.*;
import java.io.*;

/**
 * This class represents a session for persistant data storage across requests.
 *
 * @see SessionManager
 */
public class Session implements Serializable
{
	/**
	 * The cookie name used to denote the cookie holding the session ID within the HTTP request/response.
	 */
	public static final String COOKIE_NAME = "MANGO_SID";

	private String id;
	private long created;
	private long lastAccessed;
	private int maxInactiveInterval;
	private Map<String, String> attributes;

	private transient boolean isNew;
	private transient boolean invalidated;
	private transient List<SessionListener> listeners;

	/**
	 * Creates a new Session instance with the given session ID.
	 *
	 * @param id The session ID.
	 * @throws NullPointerException If id is null.
	 * @throws IllegalArgumentException If id is an empty string.
	 */
	public Session(String id)
	{
		if(id == null) {
			throw new NullPointerException();
		}
		if(id.isEmpty()) {
			throw new IllegalArgumentException("Missing id");
		}

		this.id = id;

		this.isNew = true;
		this.invalidated = false;
		this.listeners = new Vector<SessionListener>();

		this.created = System.currentTimeMillis();
		this.lastAccessed = this.created;

		this.maxInactiveInterval = (24 * 60 * 60);

		this.attributes = new HashMap<String, String>();
	}

	/**
	 * Returns the session ID.
	 *
	 * @return The session ID.
	 */
	public String getId()
	{
		return this.id;
	}

	/**
	 * Returns whether this session is new and has not yet been saved to the store by the session manager.
	 *
	 * @return True if this session is new.
	 */
	public boolean isNew()
	{
		return this.isNew;
	}

	/**
	 * Returns whether this session has been invalidated.
	 * An invalidated session can no longer be used and any attempt to read or write attributes will throw an IllegalStateException.
	 *
	 * @return True if this session has been invalidated.
	 * @see #invalidate()
	 */
	public boolean isInvalidated()
	{
		return this.invalidated;
	}

	/**
	 * Returns whether this session has expired.
	 * A session is considered to have expired when the time between the current time and the time this session was last accessed is longer than maximum inactive interval time.
	 *
	 * @return True if this session has expired.
	 * @see #setMaxInactiveInterval(int)
	 */
	public boolean hasExpired()
	{
		return ((System.currentTimeMillis() - this.lastAccessed) > (this.maxInactiveInterval * 1000));
	}

	/**
	 * Returns the date this session was created.
	 *
	 * @return The date this session was created.
	 */
	public Date getCreated()
	{
		return new Date(this.created);
	}

	/**
	 * Returns the date that this session was last accessed.
	 * A session is considered to be accessed when reading or writing attributes.
	 *
	 * @return The date that this session was last accessed.
	 */
	public Date getLastAccessed()
	{
		return new Date(this.lastAccessed);
	}

	/**
	 * Returns the maximum inactive interval time in seconds for this session.
	 *
	 * @return The maximum inactive interval time for this session.
	 * @see #hasExpired()
	 */
	public int getMaxInactiveInterval()
	{
		return this.maxInactiveInterval;
	}

	/**
	 * Sets the maximum inactive interval time in seconds for this session.
	 *
	 * @param maxInactiveInterval The maximum inactive interval time for this session.
	 * @see #hasExpired()
	 */
	public void setMaxInactiveInterval(int maxInactiveInterval)
	{
		this.maxInactiveInterval = maxInactiveInterval;
	}

	private void access()
	{
		this.lastAccessed = System.currentTimeMillis();
	}

	/**
	 * Invalidates this session.
	 * An invalidated session can no longer be used and any attempt to read or write attributes will throw an IllegalStateException.
	 *
	 * @throws IllegalStateException If this session has already been invalidated.
	 */
	public void invalidate()
	{
		if(this.invalidated) {
			throw new IllegalStateException("Session is already invalidated");
		}

		this.invalidated = true;
	}

	/**
	 * Returns whether this session is valid.
	 * A valid session has neither been invalidated or has expired.
	 *
	 * @return True if this session is valid.
	 * @see #isInvalidated()
	 * @see #hasExpired()
	 */
	public boolean isValid()
	{
		return ((!this.isInvalidated()) && (!this.hasExpired()));
	}

	/**
	 * Sets the attribute value for the given name.
	 *
	 * @param name The attribute name.
	 * @param value The attribute value.
	 * @throws IllegalStateException If this session has been invalidated.
	 * @throws NullPointerException If either name or value is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public void setAttribute(String name, String value)
	{
		if(this.invalidated) {
			throw new IllegalStateException("Invalidated session");
		}
		if((name == null) || (value == null)) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		this.access();
		this.attributes.put(name.toLowerCase(), value);
	}

	/**
	 * Returns the attribute value for the given name.
	 *
	 * @param name The attribute name.
	 * @return The attribute value for the given name.
	 * @throws IllegalStateException If this session has been invalidated.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public String getAttribute(String name)
	{
		if(this.invalidated) {
			throw new IllegalStateException("Invalidated session");
		}
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		this.access();
		return this.attributes.get(name.toLowerCase());
	}

	/**
	 * Returns whether an attribute value for the given name exists.
	 *
	 * @param name The attribute name.
	 * @return True if an attibute value for the given name exists.
	 * @throws IllegalStateException If this session has been invalidated.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public boolean hasAttribute(String name)
	{
		if(this.invalidated) {
			throw new IllegalStateException("Invalidated session");
		}
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		this.access();
		return this.attributes.containsKey(name.toLowerCase());
	}

	/**
	 * Returns a list of attribute names for this session.
	 *
	 * @return A list of attribute names for this session.
	 * @throws IllegalStateException If this session has been invalidated.
	 */
	public List<String> getAttributeNames()
	{
		if(this.invalidated) {
			throw new IllegalStateException("Invalidated session");
		}

		this.access();
		return new Vector<String>(this.attributes.keySet());
	}

	private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException
	{
		this.isNew = false;
		this.invalidated = false;
		this.listeners = new Vector<SessionListener>();

		ois.defaultReadObject();
	}

	/**
	 * Returns whether the given object is equal to this session.
	 * Sessions are considered equal if the session IDs are the same.
	 *
	 * @return True if the given object is equal to this session.
	 */
	public boolean equals(Object o)
	{
		if(!(o instanceof Session)) { return false; }
		return this.getId().equals(((Session)o).getId());
	}
}
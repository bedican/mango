/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

/**
 * <p>A listner interface for receiving session events.</p>
 * <p>The class that is interested in processing a session event either implements this interface (and all the methods it contains) or extends the abstract SessionAdapter class (overriding only the methods of interest).</p>
 *
 * @see SessionAdapter
 */
public interface SessionListener
{
	/**
	 * Invoked when a session is created.
	 *
	 * @param session The session which has been created.
	 */
	public void sessionCreated(Session session);

	/**
	 * Invoked when a session is destroyed.
	 *
	 * @param session The session which has been destroyed.
	 */
	public void sessionDestroyed(Session session);
}
/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * <p>A class loader which adds specific files and directories to the class path.</p>
 * <p>This class loader adds the directory ./ext/classes and all .jar files within ./ext/lib to the class path.</p>
 * <p>
 * To use this class loader you must remove the default system ClassLoader from the chain of delegation, this is achieved by creating an instance of this class loader using the default system ClassLoader parent as its own parent,
 * and then using this class loader to load classes within the application in its place.
 * </p>
 *
 * @see BootLoader
 * @see ClassLoader
 */
public class ServerClassLoader extends URLClassLoader
{
	/**
	 * Creates a new ServerClassLoader instance with a given class loader parent.
	 *
	 * @param parent The class loader parent.
	 */
	public ServerClassLoader(ClassLoader parent)
	{
		super(getNewURLs(), parent);
	}

	private static URL[] getNewURLs()
	{
		File libDir = new File("ext", "lib");
		File classesDir = new File("ext", "classes");

		List<URL> urls = new Vector<URL>();

		// We should parse the contents of property "java.class.path" and add each item to the class path, this will add our containing jar.

		StringTokenizer tokenizer = new StringTokenizer(System.getProperty("java.class.path"), System.getProperty("path.separator"));
		while(tokenizer.hasMoreTokens()) {
			try { urls.add((new File(tokenizer.nextToken())).toURI().toURL()); } catch(MalformedURLException mue) { mue.printStackTrace(); }
		}

		if(classesDir.isDirectory()) {
			try { urls.add(classesDir.toURI().toURL()); } catch(MalformedURLException mue) { mue.printStackTrace(); }
		}

		if(libDir.isDirectory())
		{
			File[] jarFiles = libDir.listFiles();

			for(File jarFile:jarFiles) {
				if((jarFile.isFile()) && (jarFile.getName().toLowerCase().endsWith(".jar"))) {
					try { urls.add(jarFile.toURI().toURL()); } catch(MalformedURLException mue) { mue.printStackTrace(); }
				}
			}
		}

		return urls.toArray(new URL[0]);
	}
}
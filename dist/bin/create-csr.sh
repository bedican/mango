#!/bin/bash

DIR=`dirname $0`
cd $DIR/../ssl

if [ ! -f mango.jks ]; then
	echo A keystore does not exist.
else
	keytool -certreq -alias mango -file server.csr -keystore mango.jks
fi

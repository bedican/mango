/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

/**
 * Signals an error has occured with the server configuration.
 */
public class ServerConfigException extends Exception
{
	/**
	 * Constructs a new ServerConfigException with the specified cause.
	 *
	 * @param cause The cause.
	 */
	public ServerConfigException(Throwable cause) { super(cause); }

	/**
	 * Constructs a new ServerConfigException with the specified detail message and cause.
	 *
	 * @param message The detail message.
	 * @param cause The cause.
	 */
	public ServerConfigException(String message, Throwable cause) { super(message, cause); }

	/**
	 * Constructs a new ServerConfigException with the specified detail message.
	 *
	 * @param message The detail message.
	 */
	public ServerConfigException(String message) { super(message); }
}
/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.lang.reflect.*;

/**
 * <p>
 * Provides a bootstrapping mechanism for an underlying main method using our own classloader.
 * Using our own classloader to load and invoke the main method of ServerConsole results in that classloader becoming the default throughout the application.
 * </p>
 *
 * <p>
 * We cut the default system ClassLoader (which deals with external classpath configuration) out of the chain, by using its parent as the parent of our own classloader.
 * Without removing the default system ClassLoader (due to delegation), it will always be the classloader that finds the ServerConsole class, and the one that is used when invoking its main method, which we do not want.
 * We also could not do this with the -Djava.system.class.loader="..." option, because the default system ClassLoader becomes its parent.
 * </p>
 *
 * @see ServerClassLoader
 * @see ServerConsole#main(String[])
 */
public class BootLoader
{
	private BootLoader()
	{
	}

	/**
	 * The application entry point.
	 */
	public static void main(String[] args)
	{
		try
		{
			ClassLoader loader = new ServerClassLoader(ClassLoader.getSystemClassLoader().getParent());

			Class<?> mainClass = loader.loadClass("uk.co.bedican.mango.ServerConsole");
			Method mainMethod = mainClass.getMethod("main", new Class[] {args.getClass()});

			Thread.currentThread().setContextClassLoader(loader);

			mainMethod.invoke(null, new Object[] { args });
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}
}
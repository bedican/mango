/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 *
 * This class provides a sample of extending Mango.
 */

package uk.co.bedican.mango.ext;

import uk.co.bedican.mango.*;
import java.io.*;

public class HelloWorldRequestHandler extends AbstractRequestHandler
{
	public HelloWorldRequestHandler()
	{
	}

	protected void doRequest(Request request, Response response) throws RequestHandlerException, SessionManagerException, IOException
	{
		PrintWriter out = response.getWriter();
		Session session = request.getSession();

		if(session.hasAttribute("hello"))
		{
			out.println(session.getAttribute("hello"));

			String invalidate = request.getParameter("invalidate");
			if((invalidate != null) && (invalidate.equals("1"))) {
				session.invalidate();
			}
		}
		else
		{
			session.setAttribute("hello", "hello world, Im stored in a session !");
		}
	}
}
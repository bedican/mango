/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.net.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;

/**
 * This class represents a HTTP Request.
 */
public class Request
{
	private Date date = new Date();

	private String method = "";
	private String httpVersion = "";
	private String firstLine = "";
	private String postData = "";
	
	private URI uri = null;
	private String path = null;

	private boolean parsed = false;

	private InetAddress inetAddress;
	private InputStream istream;

	private String sessionId = null;
	private SessionManager sessionManager = null;

	private Map<String, Object> attributes = new HashMap<String, Object>();
	private Map<String, String> parameters = new HashMap<String, String>();
	private Map<String, String> headers = new HashMap<String, String>();
	private Map<String, Cookie> cookies = new HashMap<String, Cookie>();

	private static Pattern requestPattern = Pattern.compile("(GET|POST|HEAD|PUT|DELETE)\\s+([^\\s]+)\\s+HTTP/(1\\.[01])");
	private static Pattern headerPattern = Pattern.compile("([^:]+):\\s*(.+)");
	private static Pattern naughtyPattern = Pattern.compile("/\\.\\.(/|\\z)");

	/**
	 * Creates a new Request instance with a given inet address and input stream.
	 *
	 * @param inetAddress The InetAddress of the client making the request.
	 * @param istream The InputStream to read the request from.
	 * @throws NullPointerException If either inetAddress or istream is null.
	 */
	public Request(InetAddress inetAddress, InputStream istream)
	{
		if((inetAddress == null) || (istream == null)) {
			throw new NullPointerException();
		}

		this.inetAddress = inetAddress;
		this.istream = istream;
	}

	/**
	 * Reads and parses the request from the internal input stream.
	 *
	 * @throws IOException If an I/O error occurs while reading the request from the client.
	 * @throws RequestException If an error occurs while parsing the request, the RequestException holds the error HttpStatus.
	 * @throws IllegalStateException If this request has already been parsed.
	 */
	public void parseRequest() throws IOException, RequestException
	{
		if(this.parsed) {
			throw new IllegalStateException("Request has already been parsed");
		}

		this.date = new Date();

		BufferedReader in = new BufferedReader(new InputStreamReader(this.istream));

		// Read first line

		String line = in.readLine();

		if(line == null) {
			throw new RequestException(HttpStatus.BAD_REQUEST);
		}

		this.firstLine = line;

		Matcher matcher = this.requestPattern.matcher(line);

		if(!matcher.matches()) {
			throw new RequestException(HttpStatus.BAD_REQUEST);
		}

		try {
			this.uri = new URI(URLDecoder.decode(matcher.group(2), "UTF-8"));
		} catch(URISyntaxException e) {
			throw new RequestException(HttpStatus.BAD_REQUEST);
		}
		
		this.path = this.uri.getPath();

		if(this.naughtyPattern.matcher(this.getPath()).find()) {
			throw new RequestException(HttpStatus.BAD_REQUEST);
		}

		this.method = matcher.group(1);
		this.httpVersion = matcher.group(3);

		if((this.method.equals("PUT")) || (this.method.equals("DELETE"))) {
			throw new RequestException(HttpStatus.METHOD_NOT_ALLOWED);
		}

		String query = this.uri.getQuery();
		if(query != null) {
			this.setParametersFromNameValuePairs(query);
		}

		// Read headers

		while(((line = in.readLine()) != null) && (!line.isEmpty()))
		{
			matcher = this.headerPattern.matcher(line);
			if(matcher.matches()) {
				this.setHeader(matcher.group(1), matcher.group(2));
			}
		}

		// Read body

		if(this.method.equals("POST"))
		{
			StringBuffer rawPostData = new StringBuffer();

			if((this.httpVersion.equals("1.0")) || ((this.httpVersion.equals("1.1")) && (this.hasHeader("connection")) && (this.getHeader("connection").toLowerCase().equals("close"))))
			{
				// Keep going until readLine returns null, the connection will close at EOF

				while((line = in.readLine()) != null) {
					rawPostData.append(line);
				}
			}
			else if((this.httpVersion.equals("1.1")) && (this.hasHeader("content-length")))
			{
				// Keep going until we have read in content-length bytes

				int contentLength = 0; char chr;

				try {
					contentLength = Integer.parseInt(this.getHeader("content-length"));
				} catch(NumberFormatException nfe) {
					throw new RequestException(HttpStatus.LENGTH_REQUIRED);
				}

				while((contentLength > 0) && ((chr = (char)in.read()) != -1))
				{
					rawPostData.append(chr);
					contentLength--;
				}
			}

			this.postData = rawPostData.toString();

			if((!this.postData.isEmpty()) && (this.hasHeader("content-type")))
			{
				if(this.getHeader("content-type").equals("application/x-www-form-urlencoded")) {
					this.setParametersFromNameValuePairs(this.postData);
				} else if(this.getHeader("content-type").startsWith("multipart/form-data; boundary=")) {
					this.parseMimeParts(this.postData);
				} else {
					throw new RequestException(HttpStatus.BAD_REQUEST);
				}
			}
		}

		this.parsed = true;
	}

	/**
	 * Returns the first line of the request headers.
	 *
	 * @return The first line of the request headers.
	 */
	public String getFirstLine()
	{
		return this.firstLine;
	}

	/**
	 * Returns the HTTP method of this request.
	 *
	 * @return The HTTP method of this request.
	 */
	public String getMethod()
	{
		return this.method;
	}

	/**
	 * Returns the HTTP version of this request.
	 *
	 * @return The HTTP version of this request.
	 */
	public String getHttpVersion()
	{
		return this.httpVersion;
	}

	/**
	 * Returns whether the HTTP method of this request is HEAD.
	 *
	 * @return True if the HTTP method of this request is HEAD.
	 */
	public boolean isMethodHead()
	{
		return this.method.equals("HEAD");
	}


    /**
     * Returns the Host header or server hostname if none was provided.
     *
     * @return The Host header or server hostname if none was provided.
     */
    public String getHost()
    {
        if(this.hasHeader("host")) {
            return this.getHeader("host");
        }

        try {
            return InetAddress.getLocalHost().getHostName();
        } catch(UnknownHostException uhe) {
            return "";
        }
    }

	/**
	 * Returns the requested path.
	 * The requested path is the original path and does not change throughout the life of the request.
	 *
	 * @return The original requested path.
	 * @throws IllegalStateException If this request does not have a URI set.
	 * @see #getPath()
	 */
	public String getRequestedPath()
	{
		return this.getUri().getPath();
	}

	/**
	 * Returns the URI of this request.
	 *
	 * @return The URI of this request.
	 * @throws IllegalStateException If this request does not have a URI set.
	 */
	public URI getUri()
	{
		if(this.uri == null) {
			throw new IllegalStateException("No URI has been set, try parsing this Request first");
		}

		return this.uri;
	}

	/**
	 * Returns the InetAddress of this request.
	 *
	 * @return The InetAddress of this request.
	 */
	public InetAddress getInetAddress()
	{
		return this.inetAddress;
	}

	/**
	 * Returns the Date this request was made.
	 *
	 * @return The Date this request was made.
	 */
	public Date getDate()
	{
		return new Date(this.date.getTime());
	}

	/**
	 * Returns the raw post data of this request.
	 * If the HTTP method of this request was not POST or there was no POST data, an empty string is returned.
	 *
	 * @return The raw post data of this request.
	 */
	public String getRawPostData()
	{
		return this.postData;
	}

	/**
	 * Returns the path element of the URI of this request.
	 * The path may change throughout the life of this request.
	 *
	 * @return The path element of the URI of this request.
	 * @throws IllegalStateException If this request does not have a path set.
	 * @see #getRequestedPath()
	 */
	public String getPath()
	{
		if(this.path == null) {
			throw new IllegalStateException("No path has been set, try parsing this Request first");
		}
		
		return this.path;
	}

	/**
	 * Returns the resolved filesystem path from the path element of the URI of this request.
	 * The path is relative to the document root and may change throughout the life of this request.
	 *
	 * @return The resolved filesystem path from the requested path.
	 */
	public String getFilePath()
	{
		String path = this.getPath();

		path = path.startsWith("/") ? path.substring(1) : path;
		path = path.replace("/", System.getProperty("file.separator"));

		return path;
	}

	/**
	 * Returns the resolved File from the path element of the URI of this request.
	 * The path is relative to the document root and may change throughout the life of this request.
	 *
	 * @return The resolved File from the requested path.
	 */
	public File getFile()
	{
		return new File(this.getFilePath());
	}

	/**
	 * Sets the path of this request.
	 *
	 * @param path The path.
	 * @throws NullPointerException If path is null.
	 * @throws IllegalArgumentException If path is an empty string.
	 */
	public void setPath(String path)
	{
		if(path == null) {
			throw new NullPointerException();
		}
		if(path.isEmpty()) {
			throw new IllegalArgumentException("Missing path");
		}

		this.path = path;
	}

	/**
	 * Sets the session manager of this request.
	 *
	 * @param manager The session manager.
	 * @throws NullPointerException If manager is null.
	 */
	public void setSessionManager(SessionManager manager)
	{
		if(manager == null) {
			throw new NullPointerException();
		}

		this.sessionManager = manager;
	}

	/* MIME data */
	private void parseMimeParts(String data)
	{
		// TODO: Parse the data as MIME parts, store the files in the tmp directory, store each file part information within the request.
	}

	/**
	 * Sets the attribute value for the given name.
	 * Request attributes are not parameters and are not sent as part of the request data.
	 *
	 * @param name The attribute name.
	 * @param value The attribute value.
	 * @throws NullPointerException If either name or value is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 * @see #setParameter(String, String)
	 * @see #getParameter(String)
	 */
	public void setAttribute(String name, Object value)
	{
		if((name == null) || (value == null)) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		this.attributes.put(name, value);
	}

	/**
	 * Returns the attribute value for the given name.
	 * Request attributes are not parameters and are not sent as part of the request data.
	 *
	 * @param name The attribute name.
	 * @return The attribute value for the given name.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 * @see #setParameter(String, String)
	 * @see #getParameter(String)
	 */
	public Object getAttribute(String name)
	{
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		return this.attributes.get(name);
	}

	/**
	 * Returns whether an attribute value for the given name exists.
	 * Request attributes are not parameters and are not sent as part of the request data.
	 *
	 * @param name The attribute name.
	 * @return True if an attibute value for the given name exists.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public boolean hasAttribute(String name)
	{
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		return this.attributes.containsKey(name);
	}

	/**
	 * Returns a list of attribute names for this request.
	 * Request attributes are not parameters and are not sent as part of the request data.
	 *
	 * @return A list of attribute names for this request.
	 */
	public List<String> getAttributeNames()
	{
		return new Vector<String>(this.attributes.keySet());
	}

	private void setParametersFromNameValuePairs(String values)
	{
		if(values == null) {
			throw new NullPointerException();
		}
		if(values.isEmpty()) {
			throw new IllegalArgumentException("Missing values");
		}

		String[] nvPairSplit;
		String[] nvPairs = values.split("&");

		for(String nvPair:nvPairs) {
			nvPairSplit = nvPair.split("=");
			if(nvPairSplit.length == 2) {
				this.setParameter(nvPairSplit[0].trim(), nvPairSplit[1].trim());
			}
		}
	}

	/**
	 * Sets the parameter value for the given name.
	 * Request parameters are sent as part of the request POST or GET data.
	 *
	 * @param name The parameter name.
	 * @param value The parameter value.
	 * @throws NullPointerException If either name or value is null.
	 */
	public void setParameter(String name, String value)
	{
		if((name == null) || (value == null)) {
			throw new NullPointerException();
		}

        // Name can be empty, e.g. ?=value
        // Even if it doesnt make much sense.

		this.parameters.put(name, value);
	}

	/**
	 * Returns the parameter value for the given name.
	 * Request parameters are sent as part of the request POST or GET data.
	 *
	 * @param name The parameter name.
	 * @return The parameter value for the given name.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public String getParameter(String name)
	{
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		return this.parameters.get(name);
	}

	/**
	 * Returns whether a parameter value for the given name exists.
	 * Request parameters are sent as part of the request POST or GET data.
	 *
	 * @param name The parameter name.
	 * @return True if a parameter value for the given name exists.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public boolean hasParameter(String name)
	{
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		return this.parameters.containsKey(name);
	}

	/**
	 * Returns a list of parameter names for this request.
	 * Request parameters are sent as part of the request POST or GET data.
	 *
	 * @return A list of parameter names for this request.
	 */
	public List<String> getParameterNames()
	{
		return new Vector<String>(this.parameters.keySet());
	}

	private void setHeader(String name, String value)
	{
		if((name == null) || (value == null)) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		if(name.toLowerCase().equals("cookie")) {
			this.setCookiesFromHeader(value);
		} else {
			this.headers.put(name.toLowerCase(), value);
		}
	}

	/**
	 * Returns the header value for the given name or null if the header does not exist.
	 *
	 * @param name The header name.
	 * @return The header value for the given name.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public String getHeader(String name)
	{
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		return this.headers.get(name.toLowerCase());
	}

	/**
	 * Returns whether a header value for the given name exists.
	 *
	 * @param name The header name.
	 * @return True if a header value for the given name exists.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public boolean hasHeader(String name)
	{
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		return this.headers.containsKey(name.toLowerCase());
	}

	/**
	 * Returns a list of header names for this request.
	 *
	 * @return A list of header names for this request.
	 */
	public List<String> getHeaderNames()
	{
		return new Vector<String>(this.headers.keySet());
	}

	private void setCookiesFromHeader(String values)
	{
		if(values == null) {
			throw new NullPointerException();
		}
		if(values.isEmpty()) {
			throw new IllegalArgumentException("Missing values");
		}

		String[] nvPairSplit;
		String[] nvPairs = values.split(";");

		for(String nvPair:nvPairs) {
			nvPairSplit = nvPair.split("=");
			if(nvPairSplit.length == 2) {
				this.addCookie(new Cookie(nvPairSplit[0].trim(), nvPairSplit[1].trim()));
			}
		}
	}

	private void addCookie(Cookie cookie)
	{
		if(cookie == null) {
			throw new NullPointerException();
		}

		this.cookies.put(cookie.getName(), cookie);
	}

	/**
	 * Returns the Cookie for the given name.
	 *
	 * @param name The cookie name.
	 * @return The cookie for the given name.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public Cookie getCookie(String name)
	{
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		return this.cookies.get(name);
	}

	/**
	 * Returns whether a cookie for the given name exists.
	 *
	 * @param name The cookie name.
	 * @return True if a cookie for the given name exists.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public boolean hasCookie(String name)
	{
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		return this.cookies.containsKey(name);
	}

	/**
	 * Returns a list of cookie names for this request.
	 *
	 * @return A list of cookie names for this request.
	 */
	public List<String> getCookieNames()
	{
		return new Vector<String>(this.cookies.keySet());
	}

	/**
	 * Returns the session ID of this request. The session ID is obtained from a session cookie, or if not present from a parameter.
	 * If no session ID could be obtained a new one is generated from this requests session manager.
	 *
	 * @return The session ID of this request.
	 * @throws IllegalStateException If no session manager is available to generate a new session ID.
	 */
	public String getSessionId()
	{
		if(this.sessionId == null)
		{
			if(this.hasCookie(Session.COOKIE_NAME)) {
				this.sessionId = this.getCookie(Session.COOKIE_NAME).getValue().trim();
			} else if(this.hasParameter(Session.COOKIE_NAME)) {
				this.sessionId = this.getParameter(Session.COOKIE_NAME).trim();
			}
			if((this.sessionId == null) || (this.sessionId.isEmpty()))
			{
				if(this.sessionManager == null) {
					throw new IllegalStateException("No SessionManager available");
				}
				this.sessionId = this.sessionManager.generateId();
			}
		}

		return this.sessionId;
	}

	/**
	 * Returns the session associated with this request, if no session exists a new one is created.
	 *
	 * @return The session associated with this request.
	 * @throws SessionManagerException If the session could not be retrieved.
	 * @throws IllegalStateException If no session manager is available to retrieve the session.
	 * @see #getSession(boolean)
	 */
	public Session getSession() throws SessionManagerException
	{
		return this.getSession(true);
	}

	/**
	 * Returns the session associated with this request or null if the create parameter is false and no session exists.
	 *
	 * @param create Whether to create a new session if the session associated with this request does not exist.
	 * @return The session associated with this request or null if create is false and no session exists.
	 * @throws SessionManagerException If the session could not be retrieved.
	 * @throws IllegalStateException If no session manager is available to retrieve the session.
	 * @see #getSession()
	 */
	public Session getSession(boolean create) throws SessionManagerException
	{
		if(this.sessionManager == null) {
			throw new IllegalStateException("No SessionManager available");
		}

		return this.sessionManager.getSession(this.getSessionId(), create);
	}
}

/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.util.*;
import java.net.*;

/**
 * Generates and sends error documents to the client.
 */
public class ErrorRequestHandler extends AbstractRequestHandler
{
	/**
	 * Creates a new ErrorRequestHandler instance.
	 */
	public ErrorRequestHandler()
	{
	}

	/**
	 * Generates and sends an error document to the client.
	 * Error information is stored within request object as a response HTTP status or in the case of a Throwable object within the request attribute &quot;error.throwable&quot;.
	 *
	 * @param request The request object holding information about the request and error.
	 * @param response The response object.
	 * @throws RequestHandlerException If the request could not be handled by this request handler.
	 * @throws SessionManagerException If there was a problem with creation or retrieval of the session associated with the request.
	 * @throws IOException If an I/O error occurs when generating the error document.
	 */
	protected void doRequest(Request request, Response response) throws RequestHandlerException, SessionManagerException, IOException
	{
		response.initHeaders();
		response.setHeader("Content-Type", "text/html");

		PrintWriter writer = response.getWriter();
		HttpStatus status = response.getStatus();

		if(!status.equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
			this.writeError(writer, status);
			return;
		}

		if(!request.hasAttribute("error.throwable")) {
			this.writeError(writer, status);
			return;
		}

		Throwable throwable;

		try {
			throwable = (Throwable)request.getAttribute("error.throwable");
		} catch(ClassCastException cce) {
			this.writeError(writer, status);
			return;
		}

		if(!this.getServerConfig().getPrintStackTrace()) {
			this.writeError(writer, status);
			return;
		}

		this.writeThrowable(writer, throwable);
	}

	/**
	 * Writes an error document to the given PrintWriter for a given HttpStatus.
	 *
	 * @param writer The PrintWriter to write the error document.
	 * @param status The HttpStatus to write to the error document.
	 */
	protected void writeError(PrintWriter writer, HttpStatus status)
	{
		writer.println("<html>");
		writer.println("<head>");
		writer.println("<title>" + status.getMessage() + "</title>");
		writer.println("</head>");
		writer.println("<body>");
		writer.println("<h1>" + status.toString() + "</h1>");
		writer.println("<div>Additionally, a 404 Not Found error was encountered while trying to use an error document to handle the request.</div>");
		writer.println("</body>");
		writer.println("</html>");
	}

	/**
	 * Writes an error document to the given PrintWriter for a given Throwable.
	 *
	 * @param writer The PrintWriter to write the error document.
	 * @param throwable The Throwable to write to the error document.
	 */
	protected void writeThrowable(PrintWriter writer, Throwable throwable)
	{
		writer.println("<html>");
		writer.println("<head>");
		writer.println("<title>" + throwable.getClass().getName() + "</title>");
		writer.println("<style type=\"text/css\">");
		writer.println("body, div { font-family:verdana; font-size:12px; }");
		writer.println("div.stack { white-space:pre; background-color:#eeeeee; margin-top:10px; margin-bottom:10px; padding:15px; }");
		writer.println("</style>");
		writer.println("</head>");
		writer.println("<body>");

		writer.println("Uh Oh!! An exception has occurred ...");
		writer.println("<div class=\"stack\">");
		throwable.printStackTrace(writer);
		writer.println("</div>");

		throwable = throwable.getCause();

		while(throwable != null)
		{
			writer.println("Cause ...");
			writer.println("<div class=\"stack\">");
			throwable.printStackTrace(writer);
			writer.println("</div>");

			throwable = throwable.getCause();
		}

		writer.println("</body>");
		writer.println("</html>");
	}
}
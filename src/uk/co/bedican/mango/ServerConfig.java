/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.net.*;
import java.io.*;
import java.util.*;

/**
 * This class represents the server configuration.
 */
public class ServerConfig
{
	private int port = 80;
	private int workers = 10;
	private int controllerPort = 8088;

	private boolean printStackTrace = false;
	private boolean displayServerSignature = true;

	private Class sessionManager = null;
	private String defaultHandlerName = null;
	private String errorHandlerName = null;

	private boolean sslEnabled = false;
	private boolean sslClientAuth = false;
	private int sslPort = 443;
	private File keystore;
	private String keystorePassword = "mangokeystore";
	private String keyPassword = "mangokey";

	private File docroot;
	private File requestLogFile;
	private File errorLogFile;

	private Map<String, RequestHandlerConfig> handlerConfigs = new HashMap<String, RequestHandlerConfig>();
	private Map<Integer, String> errorDocuments = new HashMap<Integer, String>();
	private Map<String, String> mimeTypes = new HashMap<String, String>();
	private List<String> directoryIndexes = new Vector<String>();

	/**
	 * Creates a new ServerConfig instance.
	 *
	 * @throws ServerConfigException If a new instance could not be created.
	 */
	public ServerConfig() throws ServerConfigException
	{
		try
		{
			this.setDocroot(new File("www"));
			this.setRequestLogFile(new File("request_log"));
			this.setErrorLogFile(new File("error_log"));
			this.setKeystore(new File("mango.jks"));
		}
		catch(IOException ioe)
		{
			throw new ServerConfigException("Failed to create server config", ioe);
		}
	}

	/**
	 * Returns the port to listen for HTTP requests.
	 *
	 * @return The port to listen for HTTP requests.
	 */
	public int getPort() { return this.port; }

	/**
	 * Returns the number of worker threads.
	 *
	 * @return The number of worker threads.
	 */
	public int getWorkers() { return this.workers; }

	/**
	 * Returns the controller port to listen for control requests.
	 *
	 * @return The controller port to listen for control requests.
	 */
	public int getControllerPort() { return this.controllerPort; }

	/**
	 * Returns whether to display a stack trace within error documents.
	 *
	 * @return True if an error document should display a stack trace.
	 */
	public boolean getPrintStackTrace() { return this.printStackTrace; }

	/**
	 * Returns whether to display a server signature where appropriate.
	 *
	 * @return True if the server should display a server signature where appropriate.
	 */
	public boolean getDisplayServerSignature() { return this.displayServerSignature; }

	/**
	 * Returns the document root.
	 *
	 * @return The document root.
	 */
	public File getDocroot() { return this.docroot; }

	/**
	 * Returns the File representing the request log.
	 *
	 * @return The File representing the request log.
	 */
	public File getRequestLogFile() { return this.requestLogFile; }

	/**
	 * Returns the File representing the error log.
	 *
	 * @return The File representing the error log.
	 */
	public File getErrorLogFile() { return this.errorLogFile; }

	/**
	 * Returns the Class of the session manager.
	 *
	 * @return The Class of the session manager.
	 */
	public Class getSessionManager() { return this.sessionManager; }

	/**
	 * Returns the collection of request handler configurations.
	 * The collection is an unmodifiable Map of configurations against request handler identifier names.
	 *
	 * @return The collection of request handler configurations.
	 */
	public Map<String, RequestHandlerConfig> getRequestHandlerConfigs() { return Collections.unmodifiableMap(this.handlerConfigs); }

	/**
	 * Returns the collection of MIME types.
	 * The collection is an unmodifiable Map of MIME types against file type extensions.
	 *
	 * @return The collection of MIME types.
	 */
	public Map<String, String> getMimeTypes() { return Collections.unmodifiableMap(this.mimeTypes); }

	/**
	 * Returns the collection of error documents.
	 * The collection is an unmodifiable Map of error document locations (relative to the document root) against HTTP error status codes.
	 *
	 * @return The collection of error documents.
	 */
	public Map<Integer, String> getErrorDocuments() { return Collections.unmodifiableMap(this.errorDocuments); }

	/**
	 * Returns the collection of directory indexes.
	 * The collection is an unmodifiable List of directory indexes in search order.
	 *
	 * @return The collection of directory indexes.
	 */
	public List<String> getDirectoryIndexes() { return Collections.unmodifiableList(this.directoryIndexes); }

	/**
	 * Returns whether to enable SSL. 
	 * SSL is enabled if both the enabled configuration is set and the keystore file exists.
	 *
	 * @return True if the server should listen for SSL connections.
	 */
	public boolean getSslEnabled() { return (this.sslEnabled && this.getKeystore().isFile()); }

	/**
	 * Returns whether accepted SSL connections must include successful client authentication.
	 *
	 * @return True if accepted SSL connections must include successful client authentication.
	 */
	public boolean getSslClientAuth() { return this.sslClientAuth; }

	/**
	 * Returns the port to listen for SSL HTTP requests.
	 *
	 * @return The port to listen for SSL HTTP requests.
	 */
	public int getSslPort() { return this.sslPort; }

	/**
	 * Returns the keystore.
	 *
	 * @return The File representing the keystore.
	 */
	public File getKeystore() { return this.keystore; }

	/**
	 * Returns the keystore password.
	 *
	 * @return The keystore password.
	 */
	public String getKeystorePassword() { return this.keystorePassword; }

	/**
	 * Returns the key stored within the keystore password.
	 *
	 * @return The key stored within the keystore password.
	 */
	public String getKeyPassword() { return this.keyPassword; }

	/**
	 * <p>Returns the default request handler configuration.</p>
	 * <p>If a default request handler name has not been configured, or there is no handler configuration for that name, a request handler configuration for the DefaultRequestHandler class is created and returned.</p>
	 *
	 * @return The default request handler configuration.
	 * @see DefaultRequestHandler
	 */
	public RequestHandlerConfig getDefaultRequestHandlerConfig()
	{
		if((this.defaultHandlerName == null) || (!this.handlerConfigs.containsKey(this.defaultHandlerName))) {
			return new RequestHandlerConfig("uk.co.bedican.mango.DefaultRequestHandler", DefaultRequestHandler.class);
		}

		return this.handlerConfigs.get(this.defaultHandlerName);
	}

	/**
	 * <p>Returns the error request handler configuration.</p>
	 * <p>If an error request handler name has not been configured, or there is no handler configuration for that name, a request handler configuration for the ErrorRequestHandler class is created and returned.</p>
	 *
	 * @return The error request handler configuration.
	 * @see ErrorRequestHandler
	 */
	public RequestHandlerConfig getErrorRequestHandlerConfig()
	{
		if((this.errorHandlerName == null) || (!this.handlerConfigs.containsKey(this.errorHandlerName))) {
			return new RequestHandlerConfig("uk.co.bedican.mango.ErrorRequestHandler", ErrorRequestHandler.class);
		}

		return this.handlerConfigs.get(this.errorHandlerName);
	}

	/**
	 * Sets the port to listen for HTTP requests.
	 *
	 * @param port The port to listen for HTTP requests.
	 */
	public void setPort(int port)
	{
		this.port = port;
	}

	/**
	 * Sets the number of worker threads.
	 *
	 * @param workers The number of worker threads.
	 */
	public void setWorkers(int workers)
	{
		this.workers = workers;
	}

	/**
	 * Sets the controller port to listen for control requests.
	 *
	 * @param controllerPort The controller port to listen for control requests.
	 */
	public void setControllerPort(int controllerPort)
	{
		this.controllerPort = controllerPort;
	}

	/**
	 * Sets whether to display a stack trace within error documents.
	 *
	 * @param printStackTrace True if an error document should display a stack trace.
	 */
	public void setPrintStackTrace(boolean printStackTrace)
	{
		this.printStackTrace = printStackTrace;
	}

	/**
	 * Sets whether to display a server signature where appropriate.
	 *
	 * @param displayServerSignature True if the server should display a server signature where appropriate.
	 */
	public void setDisplayServerSignature(boolean displayServerSignature)
	{
		this.displayServerSignature = displayServerSignature;
	}

	/**
	 * Sets the document root.
	 *
	 * @param docroot The document root.
	 * @throws IOException If an I/O error occurs when translating docroot in to a canonical File.
	 * @throws NullPointerException If docroot is null.
	 */
	public void setDocroot(File docroot) throws IOException
	{
		if(docroot == null) {
			throw new NullPointerException();
		}

		this.docroot = new File(docroot.getCanonicalPath());
	}

	/**
	 * Sets the File representing the request log.
	 * If the file is not absolute, it is set relative to the logs directory.
	 *
	 * @param requestLogFile The File representing the request log.
	 * @throws IOException If an I/O error occurs when translating requestLogFile in to a canonical File.
	 * @throws NullPointerException If requestLogFile is null.
	 */
	public void setRequestLogFile(File requestLogFile) throws IOException
	{
		if(requestLogFile == null) {
			throw new NullPointerException();
		}

		if(!requestLogFile.isAbsolute()) {
			requestLogFile = new File(new File("logs"), requestLogFile.getPath());
		}

		this.requestLogFile = new File(requestLogFile.getCanonicalPath());
	}

	/**
	 * Sets the File representing the error log.
	 * If the file is not absolute, it is set relative to the logs directory.
	 *
	 * @param errorLogFile The File representing the error log.
	 * @throws IOException If an I/O error occurs when translating errorLogFile in to a canonical File.
	 * @throws NullPointerException If errorLogFile is null.
	 */
	public void setErrorLogFile(File errorLogFile) throws IOException
	{
		if(errorLogFile == null) {
			throw new NullPointerException();
		}

		if(!errorLogFile.isAbsolute()) {
			errorLogFile = new File(new File("logs"), errorLogFile.getPath());
		}

		this.errorLogFile = new File(errorLogFile.getCanonicalPath());
	}

	/**
	 * Sets the default request handler name.
	 *
	 * @param defaultHandlerName The default request handler name.
	 * @throws NullPointerException If defaultHandlerName is null.
	 * @throws IllegalArgumentException If defaultHandlerName is an empty string.
	 */
	public void setDefaultRequestHandlerName(String defaultHandlerName)
	{
		if(defaultHandlerName == null) {
			throw new NullPointerException();
		}
		if(defaultHandlerName.isEmpty()) {
			throw new IllegalArgumentException("Empty handler name");
		}

		this.defaultHandlerName = defaultHandlerName;
	}

	/**
	 * Sets the error request handler name.
	 *
	 * @param errorHandlerName The error request handler name.
	 * @throws NullPointerException If errorHandlerName is null.
	 * @throws IllegalArgumentException If errorHandlerName is an empty string.
	 */
	public void setErrorRequestHandlerName(String errorHandlerName)
	{
		if(errorHandlerName == null) {
			throw new NullPointerException();
		}
		if(errorHandlerName.isEmpty()) {
			throw new IllegalArgumentException("Empty handler name");
		}

		this.errorHandlerName = errorHandlerName;
	}

	/**
	 * Sets the Class of the session manager.
	 *
	 * @param sessionManager The Class of the session manager.
	 * @throws NullPointerException If sessionManager is null.
	 */
	public void setSessionManager(Class sessionManager)
	{
		if(sessionManager == null) {
			throw new NullPointerException();
		}

		this.sessionManager = sessionManager;
	}

	/**
	 * Adds a request handler configuration.
	 *
	 * @param name The request handler identifier name.
	 * @param config The request handler configuration.
	 * @throws NullPointerException If either name or config is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public void addRequestHandlerConfig(String name, RequestHandlerConfig config)
	{
		if((name == null) || (config == null)) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Empty handler name");
		}

		this.handlerConfigs.put(name, config);
	}

	/**
	 * Adds a MIME type against file extension.
	 *
	 * @param extension The file extension.
	 * @param type The MIME type.
	 * @throws NullPointerException If either extension or type is null.
	 * @throws IllegalArgumentException If either extension or type is an empty string.
	 */
	public void addMimeType(String extension, String type)
	{
		if((extension == null) || (type == null)) {
			throw new NullPointerException();
		}
		if(extension.isEmpty()) {
			throw new IllegalArgumentException("Empty extension");
		}
		if(type.isEmpty()) {
			throw new IllegalArgumentException("Empty type");
		}

		this.mimeTypes.put(extension.toLowerCase(), type.toLowerCase());
	}

	/**
	 * Adds an error document against HTTP error status code.
	 *
	 * @param code The HTTP error status code.
	 * @param location The document location relative to the document root.
	 * @throws NullPointerException If location is null.
	 * @throws IllegalArgumentException If location is an empty string.
	 */
	public void addErrorDocument(Integer code, String location)
	{
		if(location == null) {
			throw new NullPointerException();
		}
		if(location.isEmpty()) {
			throw new IllegalArgumentException("Empty location");
		}

		this.errorDocuments.put(code, location);
	}

	/**
	 * Adds a directory index.
	 * Directory indexes should be added in search order.
	 *
	 * @param directoryIndex The directory index location.
	 * @throws NullPointerException If directoryIndex is null.
	 * @throws IllegalArgumentException If directoryIndex is an empty string.
	 */
	public void addDirectoryIndex(String directoryIndex)
	{
		if(directoryIndex == null) {
			throw new NullPointerException();
		}
		if(directoryIndex.isEmpty()) {
			throw new IllegalArgumentException("Empty directory index");
		}

		if(this.directoryIndexes.contains(directoryIndex)) {
			return;
		}

		this.directoryIndexes.add(directoryIndex);
	}

	/**
	 * Sets whether to enable SSL connections.
	 *
	 * @param sslEnabled True if SSL connections should be enabled.
	 */
	public void setSslEnabled(boolean sslEnabled)
	{
		this.sslEnabled = sslEnabled;
	}

	/**
	 * Sets whether accepted SSL connections must include successful client authentication.
	 *
	 * @param sslClientAuth True if accepted SSL connections must include successful client authentication.
	 */
	public void setSslClientAuth(boolean sslClientAuth)
	{
		this.sslClientAuth = sslClientAuth;
	}

	/**
	 * Sets the port to listen for SSL HTTP requests.
	 *
	 * @param sslPort The port to listen for SSL HTTP requests.
	 */
	public void setSslPort(int sslPort)
	{
		this.sslPort = sslPort;
	}

	/**
	 * Sets the keystore file location.
	 * If the file is not absolute, it is set relative to the ssl directory.
	 *
	 * @param keystore The keystore location.
	 * @throws IOException If an I/O error occurs when translating keystore in to a canonical File.
	 * @throws NullPointerException If keystore is null.
	 */
	public void setKeystore(File keystore) throws IOException
	{
		if(keystore == null) {
			throw new NullPointerException();
		}

		if(!keystore.isAbsolute()) {
			keystore = new File(new File("ssl"), keystore.getPath());
		}

		this.keystore = new File(keystore.getCanonicalPath());
	}

	/**
	 * Sets the keystore password.
	 *
	 * @param keystorePassword The password.
	 * @throws NullPointerException If keystorePassword is null.
	 * @throws IllegalArgumentException If keystorePassword is an empty string.
	 */
	public void setKeystorePassword(String keystorePassword)
	{
		if(keystorePassword == null) {
			throw new NullPointerException();
		}
		if(keystorePassword.isEmpty()) {
			throw new IllegalArgumentException("Empty password");
		}

		this.keystorePassword = keystorePassword;
	}

	/**
	 * Sets the key password.
	 *
	 * @param keyPassword The password.
	 * @throws NullPointerException If keyPassword is null.
	 * @throws IllegalArgumentException If keyPassword is an empty string.
	 */
	public void setKeyPassword(String keyPassword)
	{
		if(keyPassword == null) {
			throw new NullPointerException();
		}
		if(keyPassword.isEmpty()) {
			throw new IllegalArgumentException("Empty password");
		}

		this.keyPassword = keyPassword;
	}
}
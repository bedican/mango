/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.util.*;
import java.net.*;

/**
 * PHP request handler.
 * This request handler builds on the functionality of the DefaultRequestHandler to process the requested file using the PHP CGI binary.
 */
public class PhpRequestHandler extends DefaultRequestHandler
{
    private String phpPath;

	/**
	 * Creates a new PhpRequestHandler instance.
	 */
	public PhpRequestHandler()
	{
	}

	/**
	 * Initializes the request handler.
	 *
	 * @throws RequestHandlerException If an error occurs during initialization.
	 */
	protected void doInit() throws RequestHandlerException
	{
		super.doInit();

        this.phpPath = this.getInitParameter("php-cgi-bin");

        if((this.phpPath == null) || (this.phpPath.trim().isEmpty())) {
            throw new RequestHandlerException("Missing or empty php-cgi-bin configuration");
        }

        File phpFile = new File(this.phpPath);
        if(!phpFile.isFile()) {
            throw new RequestHandlerException("php-cgi-bin not found");
        }
	}

	/**
	 * <p>Interprets a file specified within the request with PHP and writes the result to the response.</p>
	 *
	 * @param request The request object holding information about the request, specifically the requested file.
	 * @param response The response object providing functionality to send a response to the client.
	 * @throws IOException If an I/O error occurs when processing the FreeMarker template.
	 */
	protected void writeFile(Request request, Response response) throws IOException
	{
        File script = new File(this.getServerConfig().getDocroot(), request.getFilePath());
        ProcessBuilder processBuilder = new ProcessBuilder(this.phpPath);

        processBuilder.directory(script.getParentFile());
        Map<String, String> env = processBuilder.environment();
        
        env.clear();

        env.put("SERVER_SOFTWARE", Server.VERSION);
        env.put("SERVER_NAME", request.getHost());
        env.put("REDIRECT_STATUS", "200");
        env.put("GATEWAY_INTERFACE", "CGI/1.1");
        env.put("SERVER_PROTOCOL", "HTTP/" + request.getHttpVersion());
        env.put("SERVER_PORT", String.valueOf(this.getServerConfig().getPort()));
        env.put("REMOTE_ADDR", request.getInetAddress().getHostAddress());        
        env.put("REQUEST_METHOD", request.getMethod());
        env.put("REQUEST_URI", request.getUri().toString());
        env.put("QUERY_STRING", (request.getUri().getQuery() != null) ? request.getUri().getQuery() : "");
        env.put("PATH_INFO", "");
        env.put("DOCUMENT_ROOT", this.getServerConfig().getDocroot().getPath());
        env.put("SCRIPT_NAME", request.getPath());
        env.put("SCRIPT_FILENAME", script.getPath());

        // Add http headers to the environment to appear in $_SERVER. Some headers should be prefixed with HTTP_

        String[] headerNames = new String[] {
        	"content-type", "content-length"
        };
        String[] headerNamesHttp = new String[] {
        	"accept", "accept-charset", "accept-encoding", "accept-language", "connection", "host", "user-agent", "referer"
        };

        for(String headerName:headerNames) {    	
        	if(request.hasHeader(headerName)) {
        		env.put(headerName.replace("-", "_").toUpperCase(), request.getHeader(headerName));
        	}
        }
        for(String headerName:headerNamesHttp) {
        	if(request.hasHeader(headerName)) {
        		env.put("HTTP_" + headerName.replace("-", "_").toUpperCase(), request.getHeader(headerName));
        	}
        }
        
        // Add cookies to environment
        
		StringBuffer cookieHeader = new StringBuffer();
		List<String> cookieNames = request.getCookieNames();
		for(String cookieName:cookieNames) {
			cookieHeader.append("; ").append(cookieName).append("=").append(request.getCookie(cookieName).getValue());
		}
		if(cookieHeader.length() != 0) {
			env.put("HTTP_COOKIE", cookieHeader.toString().substring(2));
		}

        try {
            Process process = processBuilder.start();
            
            // Send post data
            
    		if((request.getMethod().equals("POST")) && (!request.getRawPostData().isEmpty())) {
    			PrintStream outPipe = new PrintStream(new BufferedOutputStream(process.getOutputStream()), true);
    			outPipe.println(request.getRawPostData());
    		}
            
            // Obtain response
    		
            BufferedInputStream in = new BufferedInputStream(process.getInputStream());
            BufferedReader err = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            
            // Read the headers from the response
            
		    char ch; int len;
		    StringBuffer headers = new StringBuffer();

		    while((ch = (char)in.read()) != -1) {
			    headers.append(ch);
                len = headers.length();
                if(((len > 1) && (headers.substring(len - 2, len).equals("\n\n"))) || ((len > 3) && (headers.substring(len - 4, len).equals("\r\n\r\n")))) {
                    break;
                }
		    }

            String line; String[] split;
            BufferedReader inHeaders = new BufferedReader(new StringReader(headers.toString()));
		    while(((line = inHeaders.readLine()) != null) && (!line.isEmpty())) {
                split = line.split(":", 2);
                if(split.length > 1) {
                    response.addHeader(split[0].trim(), split[1].trim());
                }
		    }

            // Read the body
		    
		    OutputStream out = response.getOutputStream();

		    int bytesRead;
		    byte[] buffer = new byte[4096];

		    while((bytesRead = in.read(buffer, 0, 4096)) != -1) {
			    out.write(buffer, 0, bytesRead);
		    }

            // Read stderr

		    StringBuffer errBuffer = new StringBuffer();
		    while((line = err.readLine()) != null) {
			    errBuffer.append(line).append("\n");
		    }

            int exitCode;

            try {
                if(((exitCode = process.waitFor()) != 0) && (!errBuffer.toString().isEmpty())) {
                    throw new IOException("[ExitCode:" + exitCode + "] " + errBuffer.toString());
                }
            } catch(InterruptedException ie) {
                throw new IOException("Process interrupted", ie);
            }

        } catch(Exception e) {
            throw new IOException("Failed to process script", e);
        }
	}
}

/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.util.*;

/**
 * A work queue holds a list of Runnable objects that are run using a pool of threads as those threads become available.
 */
public final class WorkQueue
{
	private final int nThreads;
	private final PoolWorker[] pool;
	private final LinkedList<Runnable> queue;

	/**
	 * Creates a new WorkQueue instance with the given thread pool size.
	 *
	 * @param nThreads The size of the thread pool.
	 */
	public WorkQueue(int nThreads)
	{
		this.nThreads = nThreads;
		this.queue = new LinkedList<Runnable>();
		this.pool = new PoolWorker[nThreads];

		for(int i = 0; i < this.nThreads; i++)
		{
			this.pool[i] = new PoolWorker();
			this.pool[i].start();
		}
	}

	/**
	 * Adds a Runnable object to the queue.
	 *
	 * @param runnable The Runnable object to add to the queue.
	 * @throws NullPointerException if runnable is null.
	 */
	public void add(Runnable runnable)
	{
		if(runnable == null) {
			throw new NullPointerException();
		}

		synchronized(this.queue) {
			this.queue.addLast(runnable);
			this.queue.notify();
		}
	}

	/**
	 * <p>Stops all threads within the pool.</p>
	 * <p>This is to say, running threads will complete running their current Runnable object and threads waiting on the queue will stop waiting for new Runnable objects to process.</p>
	 */
	public void stop()
	{
		for(int i = 0; i < this.nThreads; i++) {
			this.pool[i].shutdown();
		}

		synchronized(this.queue) {
			this.queue.notifyAll();
		}
	}

	private class PoolWorker extends Thread
	{
		private boolean invokeShutdown = false;

		public void shutdown()
		{
			this.invokeShutdown = true;
		}

		public void run()
		{
			Runnable runnable;

			while(!this.invokeShutdown)
			{
				synchronized(queue)
				{
					while((queue.isEmpty()) && (!this.invokeShutdown))
					{
						try
						{
							queue.wait();
						}
						catch(InterruptedException ignored)
						{
						}
					}

					if(this.invokeShutdown) {
						break;
					}

					runnable = queue.removeFirst();
				}

				try
				{
					runnable.run();
				}
				catch(RuntimeException e)
				{
					// If we don't catch RuntimeException, the pool could leak threads
					// We should print the stack trace as if we were the main thread.
					e.printStackTrace();
				}
			}
		}
	}
}

/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.net.*;
import java.io.*;

/**
 * <p>A socket dispatcher listens for accepted sockets from a given ServerSocket instance and dispatches them to a given listener.</p>
 * <p>This class replaces the blocking nature of the accept method on ServerSocket with an event driven model within its own Thread.</p>
 *
 * @see ServerSocket#accept()
 */
public final class SocketDispatcher extends Thread
{
	private ServerSocket serverSocket;
	private boolean invokeShutdown = false;
	private SocketDispatcherListener listener = null;

	/**
	 * Creates a new SocketDispatcher instance with the given server socket and listener.
	 *
	 * @param serverSocket The server socket to wait for accepted sockets.
	 * @param listener The listener to dispatch sockets once accepted.
	 * @throws IOException If an I/O error occurs in creating this socket dispatcher.
	 * @throws NullPointerException If either serverSocket or listener is null.
	 */
	public SocketDispatcher(ServerSocket serverSocket, SocketDispatcherListener listener) throws IOException
	{
		if((serverSocket == null) || (listener == null)) {
			throw new NullPointerException();
		}

		this.serverSocket = serverSocket;
		this.serverSocket.setSoTimeout(4000);

		this.listener = listener;

		this.start();
	}

	/**
	 * Shuts down this socket dispatcher and stops waiting for new sockets to be accepted.
	 */
	public void shutdown()
	{
		if(!this.isAlive()) { return; }
		this.invokeShutdown = true;
	}

	/**
	 * <p>Starts listening for sockets from the encapsulated server socket. When a new socket is accepted the listener is notified.</p>
	 * <p>This method should not be called directly. Instead use start() to ensure this socket dispatcher runs within a seperate Thread.</p>
	 *
	 * @see #start()
	 */
	public void run()
	{
		while(!this.invokeShutdown)
		{
			try {
				this.listener.socketAccepted(this.serverSocket.accept());
			} catch(SocketTimeoutException ste) {
				// Just try again ...
			} catch(IOException ioe) {
				// We could do something more meaningful here.. 
				// this.listener.socketException(ioe) perhaps ?
			}
		}
	}
}
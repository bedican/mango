/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

/**
 * <p>Store and retrieves sessions on the filesystem. Sessions will be stored as serialized files within the system tmp directory or if present a tmp directory within the installation directory.</p>
 * <p>A background monitor thread runs to perform cleanup of inactive sessions from an internal memory cache and remove invalidated sessions from the filesystem.</p>
 */
public final class DefaultSessionManager extends AbstractSessionManager
{
	private File tmpDir;
	private Map<String, Session> sessions;
	private MonitorThread monitor;

	private static String FILE_EXT = ".session";

	/**
	 * Creates a new DefaultSessionManager instance.
	 * A cleanup of invalidated sessions from the filesystem is performed upon creation.
	 */
	public DefaultSessionManager()
	{
		this.tmpDir = new File("tmp");

		if(!this.tmpDir.isDirectory())
		{
			this.tmpDir = new File(System.getProperty("java.io.tmpdir"), "sessions");

			if(!this.tmpDir.isDirectory()) {
				this.tmpDir.mkdirs();
			}
		}

		this.sessions = new ConcurrentHashMap<String, Session>();

		this.deleteFiles();

		this.monitor = new MonitorThread(this);
		this.monitor.start();
	}

	/**
	 * Retrieves a session from the filesystem from a given session ID.
	 * If the session does not exist and the create parameter is true, a new session will be returned, otherwise null.
	 *
	 * @param sessionId The ID of the session to retrieve.
	 * @param create Whether to create a new session if the session for the given ID does not exist.
	 * @return A session for the given ID or if no session is available a new session if the create parameter is true otherwise null.
	 * @throws SessionManagerException If there was a problem with creation or retrieval of the session.
	 */
	protected Session load(String sessionId, boolean create) throws SessionManagerException
	{
		Session session = this.sessions.get(sessionId);

		if((session != null) && (session.isValid())) {
			return session;
		}

		if(session != null) {
			this.delete(session);
		}

		try {
			session = this.loadFromFile(new File(this.tmpDir, sessionId + FILE_EXT));
		} catch(IOException ioe) {
			throw new SessionManagerException(ioe);
		}

		if((session != null) && (session.isValid())) {
			this.sessions.put(sessionId, session);
			return session;
		}

		if(!create) {
			return null;
		}

		session = new Session(sessionId);
		this.sessions.put(sessionId, session);

		return session;
	}

	private Session loadFromFile(File file) throws IOException
	{
		if(!file.isFile()) {
			return null;
		}

		try
		{
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);

			Session session = (Session)ois.readObject();

			ois.close();

			return session;
		}
		catch(IOException ioe)
		{
			throw ioe;
		}
		catch(Exception e)
		{
			return null;
		}
	}

	/**
	 * Saves a given session to the filesystem.
	 *
	 * @param session The session to save.
	 * @throws SessionManagerException If there was a problem with saving the session.
	 */
	protected void save(Session session) throws SessionManagerException
	{
		try
		{
			File file = new File(this.tmpDir, session.getId() + FILE_EXT);

			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			oos.writeObject(session);

			oos.close();
		}
		catch(IOException ioe)
		{
			throw new SessionManagerException(ioe);
		}
	}

	/**
	 * Removes a given session from the filesystem.
	 *
	 * @param session The session to remove.
	 * @throws SessionManagerException If there was a problem with removing the session.
	 */
	protected void delete(Session session) throws SessionManagerException
	{
		File file = new File(this.tmpDir, session.getId() + FILE_EXT);

		if(!file.isFile()) {
			return;
		}

		if(!file.delete()) {
			throw new SessionManagerException("Failed to delete " + file.getAbsolutePath());
		}

		this.sessions.remove(session.getId());
	}

	/**
	 * Called on server shutdown.
	 */
	public void shutdown()
	{
		this.monitor.shutdown();
	}

	private void deleteCache()
	{
		// Remove sessions from the memory cache, this does not mean the sessions are invalid, we are just reducing memory usage.

		Set<String> keys = this.sessions.keySet();

		for(String key:keys)
		{
			if(this.sessions.get(key).getLastAccessed().getTime() > (System.currentTimeMillis() + (10 * 60 * 1000))) {
				this.sessions.remove(key);
			}
		}
	}
	private void deleteFiles()
	{
		// Delete sessions from the file system, if they have become invalidated.

		Session session;
		File[] files = this.tmpDir.listFiles();

		for(File file:files)
		{
			if((!file.isFile()) || (!file.getName().endsWith(FILE_EXT))) {
				continue;
			}

			try {
				session = this.loadFromFile(file);
			} catch(IOException ioe) {
				continue;
			}

			if(session == null) {
				continue;
			}

			if(this.sessions.containsKey(session.getId())) {
				continue;
			}

			if(session.isValid()) {
				continue;
			}

			try {
				this.removeSession(session);
			} catch(SessionManagerException sme) {
			}
		}
	}

	private static class MonitorThread extends Thread
	{
		private DefaultSessionManager manager;
		private boolean invokeShutdown = false;

		public MonitorThread(DefaultSessionManager manager)
		{
			this.manager = manager;
		}

		public void shutdown()
		{
			this.invokeShutdown = true;
			this.interrupt();
		}

		public void run()
		{
			int count = 0;

			while(!this.invokeShutdown)
			{
				try {
					Thread.sleep(15 * 60 * 1000);
				} catch(InterruptedException ie) {
					continue;
				}

				// Every 15 minutes, remove old sessions from the cache

				count++;
				this.manager.deleteCache();

				if(count < 24) {
					continue;
				}

				// Every 6 hours, tidy up the filesystem

				count = 0;
				this.manager.deleteFiles();
			}
		}
	}
}
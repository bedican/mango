@echo off
cd %~DP0/../ssl

IF EXIST mango.jks (
	echo A keystore already exists.
) else (
	keytool -genkey -alias mango -keyalg RSA -keysize 2048 -keystore mango.jks
)

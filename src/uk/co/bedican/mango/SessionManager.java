/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

/**
 * A session manager provides an interface to the session storage mechanism.
 *
 * @see Session
 */
public interface SessionManager
{
	/**
	 * Return a new unique session ID.
	 *
	 * @return A unique session ID.
	 */
	public String generateId();

	/**
	 * Retrieve a session from the store from a given session ID.
	 * If the session does not exist and the create parameter is true, a new session will be returned, otherwise null.
	 *
	 * @param sessionId The ID of the session to retrieve.
	 * @param create Whether to create a new session if the session for the given ID does not exist.
	 * @return A session for the given ID or if no session is available a new session if the create parameter is true otherwise null.
	 * @throws SessionManagerException If there was a problem with creation or retrieval of the session.
	 */
	public Session getSession(String sessionId, boolean create) throws SessionManagerException;

	/**
	 * Save a given session to the store.
	 *
	 * @param session The session object to save.
	 * @throws SessionManagerException If there was a problem with saving the session.
	 */
	public void putSession(Session session) throws SessionManagerException;

	/**
	 * Remove a given session from the store.
	 *
	 * @param session The session object to remove.
	 * @throws SessionManagerException If there was a problem with removing the session.
	 */
	public void removeSession(Session session) throws SessionManagerException;

	/**
	 * Adds a session listener to the session manager.
	 *
	 * @param listener The session listener.
	 */
	public void addListener(SessionListener listener);

	/**
	 * Removes a session listener from the session manager.
	 *
	 * @param listener The session listener.
	 */
	public void removeListener(SessionListener listener);

	/**
	 * Called on server shutdown.
	 */
	public void shutdown();
}
/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.text.*;
import java.util.*;

/**
 * Defines common functionality for loggers.
 */
abstract public class AbstractLogger
{
	private PrintStream out;
	private static SimpleDateFormat sdf = new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss]");

	/**
	 * Initializes the PrintStream for this AbstractLogger with a specified File. If the PrintStream could not be created using the given File a fallback of System.out is used.
	 *
	 * @param file The file used to create the PrintStream.
	 * @throws NullPointerException if file is null.
	 */
	protected void init(File file)
	{
		if(file == null) {
			throw new NullPointerException();
		}

		try {
			this.out = new PrintStream(new FileOutputStream(file, true), true);
		} catch(FileNotFoundException fnfe) {
			this.out = System.out;
			this.out.println("Failed to init class " + this.getClass().getName() + ", logging to stdout instead ...");
		}
	}

	/**
	 * Returns the PrintStream for this AbstractLogger.
	 *
	 * @return The PrintStream.
	 */
	protected PrintStream getPrintStream()
	{
		return this.out;
	}

	/**
	 * Format a given Date using the format [yyyy-MM-dd HH:mm:ss].
	 *
	 * @param date The Date to format.
	 * @return The formatted Date as String.
	 * @throws NullPointerException if date is null.
	 */
	protected String formatDate(Date date)
	{
		if(date == null) {
			throw new NullPointerException();
		}

		return this.sdf.format(date);
	}
}

/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.util.*;
import java.net.*;

/**
 * Send the requested file or directory index to the client.
 */
public class DefaultRequestHandler extends AbstractRequestHandler
{
	/**
	 * Creates a new DefaultRequestHandler instance.
	 */
	public DefaultRequestHandler()
	{
	}

	/**
	 * Writes a file specified within the request to the response output stream.
	 *
	 * @param request The request object holding information about the request, specifically the requested file.
	 * @param response The response object providing functionality to send a response to the client.
	 * @throws IOException If an I/O error occurs when reading or writing the requested file.
	 */
	protected void writeFile(Request request, Response response) throws IOException
	{
		File resource = new File(this.getServerConfig().getDocroot(), request.getFilePath());

		OutputStream out = response.getOutputStream();
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(resource));

		int bytesRead;
		byte[] buffer = new byte[4096];

		while((bytesRead = in.read(buffer, 0, 4096)) != -1) {
			out.write(buffer, 0, bytesRead);
		}

		in.close();
	}

	/**
	 * Sends the requested file to the client.
	 * If a directory is specified the file is resolved using the server directory index configuration.
	 * If the file is not found a 404 HTTP response is sent to the client or if no directory index could be located a 403.
	 *
	 * @param request The request object holding information about the request.
	 * @param response The response object providing functionality to send a response to the client.
	 * @throws RequestHandlerException If the request could not be handled by this request handler.
	 * @throws SessionManagerException If there was a problem with creation or retrieval of the session associated with the request.
	 * @throws IOException If an I/O error occurs when handling the request.
	 */
	protected void doRequest(Request request, Response response) throws RequestHandlerException, SessionManagerException, IOException
	{
		File resource = new File(this.getServerConfig().getDocroot(), request.getFilePath());

		if(!resource.getPath().equals(resource.getCanonicalPath())) {
			response.sendError(HttpStatus.NOT_FOUND);
			return;
		}

		if((resource.isFile()) && (request.getPath().endsWith("/"))) {
			response.sendError(HttpStatus.NOT_FOUND);
			return;
		}

		if(resource.isHidden()) {
			response.sendError(HttpStatus.NOT_FOUND);
			return;
		}

		if(resource.isFile())
		{
			response.setHeader("Content-Type", this.getContext().getMimeType(resource));
			this.writeFile(request, response);
		}
		else if(resource.isDirectory())
		{
			if(!request.getPath().endsWith("/")) {
				response.redirect(request.getPath() + "/");
				return;
			}

			File indexFile;

			List<String> directoryIndexes = this.getServerConfig().getDirectoryIndexes();

			for(String directoryIndex:directoryIndexes)
			{
				indexFile = new File(resource, directoryIndex);
				if(indexFile.isFile()) {
					this.getContext().getRequestDispatcher(request.getPath() + directoryIndex).forward(request, response);
					return;
				}
			}

			// TODO: directory listing ?
			response.sendError(HttpStatus.FORBIDDEN);
		}
		else
		{
			response.sendError(HttpStatus.NOT_FOUND);
		}
	}
}
/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

/**
 * Signals an error has occured in the session manager.
 */
public class SessionManagerException extends Exception
{
	/**
	 * Constructs a new SessionManagerException with the specified cause.
	 *
	 * @param cause The cause.
	 */
	public SessionManagerException(Throwable cause) { super(cause); }

	/**
	 * Constructs a new SessionManagerException with the specified detail message and cause.
	 *
	 * @param message The detail message.
	 * @param cause The cause.
	 */
	public SessionManagerException(String message, Throwable cause) { super(message, cause); }

	/**
	 * Constructs a new SessionManagerException with the specified detail message.
	 *
	 * @param message The detail message.
	 */
	public SessionManagerException(String message) { super(message); }
}
/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.net.*;
import javax.net.ssl.*;
import java.security.*;

/**
 * The server instance.
 */
public class Server
{
	/**
	 * The server version.
	 */
	public static final String VERSION = "Mango " + Server.class.getPackage().getImplementationVersion();

	private ServerConfig config;

	private WorkQueue workQueue;
	private ServerContext context;
	private SocketDispatcher dispatcher;
	private SocketDispatcher sslDispatcher;

	private boolean isAlive = false;

	private List<ServerListener> listeners = new Vector<ServerListener>();

	/**
	 * Creates a new Server instance with a given configuration.
	 *
	 * @param config the server configuration.
	 * @throws NullPointerException If config is null.
	 */
	public Server(ServerConfig config)
	{
		if(config == null) {
			throw new NullPointerException();
		}

		this.config = config;
	}

	/**
	 * Returns the server configuration.
	 *
	 * @return The server configuration.
	 */
	public ServerConfig getConfig()
	{
		return this.config;
	}

	/**
	 * Adds a server listener to this server.
	 *
	 * @param listener The server listener.
	 * @throws NullPointerException If listener is null.
	 */
	public void addListener(ServerListener listener)
	{
		if(listener == null) {
			throw new NullPointerException();
		}

		this.listeners.add(listener);
	}

	private void fireServerStarted()
	{
		for(ServerListener listener:this.listeners) {
			listener.serverStarted(this);
		}
	}
	private void fireServerStopping()
	{
		for(ServerListener listener:this.listeners) {
			listener.serverStopping(this);
		}
	}
	private void fireServerStopped()
	{
		for(ServerListener listener:this.listeners) {
			listener.serverStopped(this);
		}
	}

	/**
	 * Shuts down the server. If the server is not running this method has no effect.
	 */
	public void shutdown()
	{
		if(!this.isAlive) {
			return;
		}

		this.fireServerStopping();
		this.isAlive = false;
		this.fireServerStopped();
	}

	/**
	 * Starts the server. If the server is already running this method has no effect.
	 */
	public void start()
	{
		if(this.isAlive) {
			return;
		}

		try
		{
			this.workQueue = new WorkQueue(this.config.getWorkers());
			this.addListener(new ServerAdapter() {
				public void serverStopping(Server server) {
					workQueue.stop();
				}
			});

			this.context = new ServerContext(this.config);
			this.addListener(new ServerAdapter() {
				public void serverStopping(Server server) {
					context.shutdown();
				}
			});

			SocketDispatcherListener socketListener = new SocketDispatcherListener() {
				public void socketAccepted(Socket socket) {
					workQueue.add(new SocketHandler(context, socket));
				}
			};

			this.dispatcher = new SocketDispatcher(new ServerSocket(this.config.getPort()), socketListener);
			this.addListener(new ServerAdapter() {
				public void serverStopping(Server server) {
					dispatcher.shutdown();
				}
			});

			if(this.config.getSslEnabled())
			{
				this.sslDispatcher = new SocketDispatcher(this.getSslServerSocket(), socketListener);
				this.addListener(new ServerAdapter() {
					public void serverStopping(Server server) {
						sslDispatcher.shutdown();
					}
				});
			}

			this.isAlive = true;
			this.fireServerStarted();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			this.shutdown();
		}
	}

	private ServerSocket getSslServerSocket() throws Exception
	{
		FileInputStream in = new FileInputStream(this.config.getKeystore());

		KeyStore keyStore = KeyStore.getInstance("JKS");
		keyStore.load(in, this.config.getKeystorePassword().toCharArray());
		in.close();

		KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
		keyManagerFactory.init(keyStore, this.config.getKeyPassword().toCharArray());

		SSLContext sslContext = SSLContext.getInstance("SSLv3");
		sslContext.init(keyManagerFactory.getKeyManagers(), null, null);

		ServerSocketFactory serverSocketFactory = sslContext.getServerSocketFactory();

		SSLServerSocket serverSocket = (SSLServerSocket)serverSocketFactory.createServerSocket(this.config.getSslPort());
		serverSocket.setNeedClientAuth(this.config.getSslClientAuth());

		return serverSocket;
	}
}
/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.util.*;
import java.io.*;
import java.text.*;

/**
 * This class represents a HTTP Response.
 */
public class Response implements SessionListener
{
	private static enum StreamStatus { UNCOMMITED, COMMITED, CLOSED; }

	private Request request = null;
	private ServerContext context = null;

	private OutputStream outputStream;
	private ByteArrayOutputStream byteArrayOutputStream;
	private PrintWriter printWriter;

	private HttpStatus status = HttpStatus.OK;
	private HttpStatus lastSendErrorStatus = null;
	private StreamStatus streamStatus = StreamStatus.UNCOMMITED;
	private boolean sendHeadersOnly = false;
	private int bytesWritten = 0;

	private Map<String, KeyList> headers = new HashMap<String, KeyList>();
	private Map<String, Cookie> cookies = new HashMap<String, Cookie>();

	private static String CRLF = "\r\n";
	private static SimpleDateFormat SDF = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z");

	/**
	 * Creates a new Response instance with a given output stream.
	 *
	 * @param ostream The output stream used to send data to the client.
	 * @throws NullPointerException If ostream is null.
	 */
	public Response(OutputStream ostream)
	{
		if(ostream == null) {
			throw new NullPointerException();
		}

		this.outputStream = ostream;
		this.byteArrayOutputStream = new ByteArrayOutputStream();
		this.printWriter = new PrintWriter(this.byteArrayOutputStream, true);
	}

	/**
	 * Sets the request of this response.
	 *
	 * @param request The request.
	 * @throws NullPointerException If request is null.
	 */
	public void setRequest(Request request)
	{
		if(request == null) {
			throw new NullPointerException();
		}

		this.request = request;
	}

	/**
	 * Sets the server context of this response.
	 *
	 * @param context The server context.
	 * @throws NullPointerException If context is null.
	 */
	public void setContext(ServerContext context)
	{
		if(context == null) {
			throw new NullPointerException();
		}

		this.context = context;
	}

	/**
	 * Sets the HTTP status of this response.
	 *
	 * @param status The HTTP status.
	 * @throws NullPointerException If status is null.
	 */
	public void setStatus(HttpStatus status)
	{
		if(status == null) {
			throw new NullPointerException();
		}

		this.status = status;
	}

	/**
	 * Returns the HTTP status of this response.
	 *
	 * @return The HTTP status of this response.
	 */
	public HttpStatus getStatus()
	{
		return this.status;
	}

	/**
	 * Returns the number of bytes currently sent to the client.
	 *
	 * @return The number of bytes currently sent to the client.
	 */
	public int getBytesWritten()
	{
		return this.bytesWritten;
	}

	/**
	 * Sets that this response should send HTTP headers only.
	 *
	 * @throws IllegalStateException If body data has already been sent to the client.
	 */
	public void sendHeadersOnly()
	{
		if(this.bytesWritten > 0) {
			throw new IllegalStateException("This response has already commited body data to the client.");
		}

		this.sendHeadersOnly = true;
	}

	/**
	 * Returns the output stream of this response.
	 * Data written to this output stream is buffered until flushBuffer() is called where it is then sent to the client.
	 *
	 * @return The output stream of this response.
	 * @see #flushBuffer()
	 * @see #clearBuffer()
	 */
	public OutputStream getOutputStream()
	{
		return this.byteArrayOutputStream;
	}

	/**
	 * Returns an auto-flushing print writter writing to the output stream of this response.
	 *
	 * @return An auto-flushing print writer writing to the output stream of this response.
	 * @see #getOutputStream()
	 */
	public PrintWriter getWriter()
	{
		return this.printWriter;
	}

	/**
	 * Returns whether data has been sent to the client.
	 *
	 * @return True if data has been sent to the client.
	 */
	public boolean isCommited()
	{
		return (!this.streamStatus.equals(StreamStatus.UNCOMMITED));
	}

	/**
	 * Closes the response. Once a response is closed no further data is sent to the client.
	 */
	public void close()
	{
		this.streamStatus = StreamStatus.CLOSED;
	}

	/**
	 * Clears the buffers of this response.
	 *
	 * @see #flushBuffer()
	 */
	public void clearBuffer()
	{
		this.printWriter.flush();
		this.byteArrayOutputStream.reset();
	}

	/**
	 * <p>Flushes the buffers of this response and sends the data to the client.</p>
	 * <p>Response headers are sent to the client on the first call of this method resulting in further changes to the response headers having no effect.</p>
	 * <p>If the response has been closed no data is sent to the client, however the response buffer is still cleared.</p>
	 *
	 * @throws IOException If an I/O error occurs when sending data to the client.
	 * @see #clearBuffer()
	 * @see #isCommited()
	 */
	public void flushBuffer() throws IOException
	{
		if(!this.isCommited())
		{
			this.streamStatus = StreamStatus.COMMITED;

			PrintWriter out = new PrintWriter(this.outputStream, false);

			out.print("HTTP/1.0 " + this.status.toString() + CRLF);

			// Headers

			Set<String> headerKeys = this.headers.keySet();
			for(String name:headerKeys) {
                KeyList header = this.headers.get(name);
				for(String value:header.getValues()) {
					out.print(header.getName() + ": " + value + CRLF);
				}
			}

			// Cookies

			Set<String> cookieKeys = this.cookies.keySet();
			for(String name:cookieKeys) {
				out.print("Set-Cookie: " + this.cookies.get(name).toString() + CRLF);
			}

			out.print(CRLF);
			out.flush();
		}
		if((!this.streamStatus.equals(StreamStatus.CLOSED)) && (!this.sendHeadersOnly))
		{
			this.printWriter.flush();
			this.byteArrayOutputStream.writeTo(this.outputStream);
			this.bytesWritten += this.byteArrayOutputStream.size();
		}

		this.clearBuffer();
	}

	/**
	 * Sends an error document to the client for a given HTTP status. Control is forwarded to an error document request handler.
	 *
	 * @param status The HTTP status.
	 * @throws RequestHandlerException If the request could not be handled by the error document request handler.
	 * @throws SessionManagerException If there was a problem with creation or retrieval of the session associated with the original request.
	 * @throws IOException If an I/O error occurs with the error document request handler.
	 * @throws IllegalStateException If the response has already been commited to the client or this response does not have server context or request objects available.
	 * @throws NullPointerException If status is null.
	 * @throws IllegalArgumentException If status is not a HTTP error status.
	 * @see #setContext(ServerContext)
	 * @see #setRequest(Request)
	 * @see HttpStatus#isError()
	 * @see ServerContext#getErrorDocumentDispatcher(HttpStatus)
	 */
	public void sendError(HttpStatus status) throws RequestHandlerException, SessionManagerException, IOException
	{
		if(this.isCommited()) {
			throw new IllegalStateException("This response has already commited data to the client.");
		}

		if(status == null) {
			throw new NullPointerException();
		}
		if(!status.isError()) {
			throw new IllegalArgumentException("Status is not an error");
		}

		if((this.context == null) || (this.request == null)) {
			throw new IllegalStateException("Missing context or request objects");
		}

		this.initHeaders();

		if((this.lastSendErrorStatus != null) && (status.equals(HttpStatus.NOT_FOUND)))
		{
			this.setStatus(this.lastSendErrorStatus);
			this.context.getErrorDocumentDispatcher().forward(this.request, this);
			return;
		}

		this.lastSendErrorStatus = status;

		this.setStatus(status);
		this.context.getErrorLogger().log(this.request, this);
		this.context.getErrorDocumentDispatcher(status).forward(this.request, this);

	}

	/**
	 * Sends an error document to the client for a given Throwable object.
	 * The Throwable object is stored as a request attribute &quot;error.throwable&quot; before forwarding control to an error document request handler.
	 *
	 * @param throwable The Throwable object.
	 * @throws IllegalStateException If the response has already been commited to the client or this response does not have server context or request objects available.
	 * @see #setContext(ServerContext)
	 * @see #setRequest(Request)
	 */
	public void sendThrowable(Throwable throwable)
	{
		if(this.isCommited()) {
			throw new IllegalStateException("This response has already commited data to the client.", throwable);
		}
		if(this.request == null) {
			throw new IllegalStateException("Missing request object");
		}

		if(throwable == null) {
			throwable = new NullPointerException();
		}

		try
		{
			this.request.setAttribute("error.throwable", throwable);
			this.sendError(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		catch(Exception e)
		{
			// Trap here, or it will just be recursive. It is already logged to the error logger within sendError.
		}
	}

	/**
	 * <p>Sends a temporary redirect to the client. Any data written to the response buffer is cleared and replaced with a Location HTTP header.</p>
	 * <p>Calling this method is equivalent to calling this.redirect(location, false)</p>
	 *
	 * @param location The location to redirect to.
	 * @throws IOException If an I/O error occurs when sending data to the client.
	 * @throws IllegalStateException If the response has already been commited to the client.
	 * @throws NullPointerException If location is null.
	 * @throws IllegalArgumentException If location is an empty string.
	 * @see #redirect(String, boolean)
	 */
	public void redirect(String location) throws IOException
	{
		this.redirect(location, false);
	}

	/**
	 * Sends a redirect to the client. Any data written to the response buffer is cleared and replaced with a Location HTTP header.
	 *
	 * @param location The location to redirect to.
	 * @param permanent True if the redirect should be permanent (HTTP status 301).
	 * @throws IOException If an I/O error occurs when sending data to the client.
	 * @throws IllegalStateException If the response has already been commited to the client.
	 * @throws NullPointerException If location is null.
	 * @throws IllegalArgumentException If location is an empty string.
	 * @see #redirect(String)
	 */
	public void redirect(String location, boolean permanent) throws IOException
	{
		if(this.isCommited()) {
			throw new IllegalStateException("This response has already commited data to the client.");
		}
		if(location == null) {
			throw new NullPointerException();
		}
		if(location.isEmpty()) {
			throw new IllegalArgumentException("Missing location");
		}

		this.initHeaders();
		this.clearBuffer();

		this.sendHeadersOnly();

		if(permanent) {
			this.setStatus(HttpStatus.MOVED_PERMANENTLY);
		} else {
			this.setStatus(HttpStatus.TEMPORARY_REDIRECT);
		}

		this.setHeader("Location", location);

		this.flushBuffer();
		this.close();
	}

	/**
	 * Clears the HTTP headers for this response.
	 *
	 * @throws IllegalStateException If the response has already been commited to the client.
	 * @see #initHeaders()
	 */
	public void clearHeaders()
	{
		if(this.isCommited()) {
			throw new IllegalStateException("This response has already commited data to the client.");
		}

		this.headers.clear();
	}

	/**
	 * Initializes the HTTP headers for this response.
	 *
	 * @throws IllegalStateException If this response does not have server context or request objects available or if the response has already been commited to the client.
	 * @see #clearHeaders()
	 * @see #setContext(ServerContext)
	 * @see #setRequest(Request)
	 */
	public void initHeaders()
	{
		if(this.isCommited()) {
			throw new IllegalStateException("This response has already commited data to the client.");
		}
		if((this.context == null) || (this.request == null)) {
			throw new IllegalStateException("Missing context or request objects");
		}

		// TODO: this could be done in flushBuffer() before sending headers, however would be less visible of whats happening.
		// If so, all calls to initHeaders() can be replaced with clearHeaders().

		this.clearHeaders();

		this.setHeader("Date", SDF.format(new Date()));
		this.setHeader("Connection", "close");

		if(this.context.getConfig().getDisplayServerSignature()) {
			this.setHeader("Server", Server.VERSION);
		}
	}

	/**
	 * <p>Sets the response header value for the given name. If the header name already exists, it is replaced.</p>
	 *
	 * @param name The header name.
	 * @param value The header value.
	 * @throws IllegalStateException If the response has already been commited to the client.
	 * @throws NullPointerException If either name or value is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public void setHeader(String name, String value)
	{
		this.addHeader(name, value, true);
	}

	/**
	 * <p>Adds a header value for the given name to this response. If the header name already exists, it is added as an additional value for that header name.</p>
	 *
	 * @param name The header name.
	 * @param value The header value.
	 * @throws IllegalStateException If the response has already been commited to the client.
	 * @throws NullPointerException If either name or value is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public void addHeader(String name, String value)
	{
		this.addHeader(name, value, false);
	}

	private void addHeader(String name, String value, boolean clear)
	{
		if(this.isCommited()) {
			throw new IllegalStateException("This response has already commited data to the client.");
		}
		if((name == null) || (value == null)) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		KeyList values;

		if(this.headers.containsKey(name.toLowerCase())) {
			values = this.headers.get(name.toLowerCase());
		} else {
			values = new KeyList(name);
		}

		if(clear) {
			values.clear();
		}

		values.add(value);

		this.headers.put(name.toLowerCase(), values);
	}

	/**
	 * Removes a header from this response.
	 *
	 * @param name The header name.
	 * @throws IllegalStateException If the response has already been commited to the client.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public void removeHeader(String name)
	{
		if(this.isCommited()) {
			throw new IllegalStateException("This response has already commited data to the client.");
		}
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		this.headers.remove(name.toLowerCase());
	}

	/**
	 * Returns the list of header values for the given name or null if the header does not exist.
	 *
	 * @param name The header name.
	 * @return The list of header values for the given name.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public List<String> getHeader(String name)
	{
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		if(!this.headers.containsKey(name.toLowerCase())) {
			return null;
		}

		return this.headers.get(name.toLowerCase()).getValues();
	}

	/**
	 * Returns whether a header value for the given name exists.
	 *
	 * @param name The header name.
	 * @return True if a header value for the given name exists.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public boolean hasHeader(String name)
	{
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		return this.headers.containsKey(name.toLowerCase());
	}

	/**
	 * Adds a cookie to this response.
	 *
	 * @param cookie The cookie.
	 * @throws IllegalStateException If the response has already been commited to the client.
	 * @throws NullPointerException If cookie is null.
	 */
	public void addCookie(Cookie cookie)
	{
		if(this.isCommited()) {
			throw new IllegalStateException("This response has already commited data to the client.");
		}
		if(cookie == null) {
			throw new NullPointerException();
		}

		this.cookies.put(cookie.getName(), cookie);
	}

	/**
	 * Clears the cookies for this response.
	 */
	public void clearCookies()
	{
		this.cookies.clear();
	}

	/**
	 * Listens for created sessions. A new cookie is added to this response with the given session ID.
	 *
	 * @param session The created session.
	 * @see AbstractSessionManager#getSession(String, boolean)
	 */
	public void sessionCreated(Session session)
	{
		if(this.isCommited()) {
			return;
		}
		if(this.request == null) {
			return;
		}

		String sessionId = this.request.getSessionId();

		if(!session.getId().equals(sessionId)) {
			return;
		}

		this.addCookie(new Cookie(Session.COOKIE_NAME, session.getId()));
	}

	/**
	 * Listens for destroyed sessions. A new cookie is added to this response with the given session ID set to expire with the current date.
	 *
	 * @param session The destroyed session.
	 * @see AbstractSessionManager#removeSession(Session)
	 */
	public void sessionDestroyed(Session session)
	{
		if(this.isCommited()) {
			return;
		}
		if(this.request == null) {
			return;
		}

		String sessionId = this.request.getSessionId();

		if(!session.getId().equals(sessionId)) {
			return;
		}

		Cookie cookie = new Cookie(Session.COOKIE_NAME, session.getId());
		cookie.setExpires(new Date());

		this.addCookie(cookie);
	}

	/**
	 * Simple class to represent a header name / value pair.
	 * Used to maintain case-insensitive header names without loosing the original case of the name.
	 */
	private static class KeyList
	{
		private String name;
		private List<String> values;

		public KeyList(String name)
		{
			this.name = name;
			this.values = new Vector<String>();
		}
		public String getName()
		{
			return this.name;
		}
		public List<String> getValues()
		{
			return Collections.unmodifiableList(this.values);
		}
		public void add(String value)
		{
            if(this.values.contains(value)) {
                return;
            }

			this.values.add(value);
		}
		public void clear()
		{
			this.values.clear();
		}
	}
}

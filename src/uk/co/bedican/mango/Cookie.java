/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.util.*;

/**
 * This class represents a HTTP cookie and its attributes.
 */
public class Cookie
{
	private String name = null;
	private String value = null;;
	private String comment = null;;
	private String path = null;;
	private String domain = null;;
	private boolean secure = false;
	private Date expires = null;

	/**
	 * Creates a new Cookie instance with the given name and value.
	 *
	 * @param name The cookie name.
	 * @param value The cookie value.
	 * @throws NullPointerException If either name or value is null.
	 * @throws IllegalArgumentException If either name or value is an empty string.
	 */
	public Cookie(String name, String value)
	{
		this.setName(name);
		this.setValue(value);
	}

	/**
	 * Returns the cookie name.
	 *
	 * @return The cookie name.
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Returns the cookie value.
	 *
	 * @return The cookie value.
	 */
	public String getValue()
	{
		return this.value;
	}

	/**
	 * Returns the cookie comment.
	 *
	 * @return The cookie comment.
	 */
	public String getComment()
	{
		return this.comment;
	}

	/**
	 * Returns the cookie path.
	 *
	 * @return The cookie path.
	 */
	public String getPath()
	{
		return this.path;
	}

	/**
	 * Returns the cookie domain.
	 *
	 * @return The cookie domain.
	 */
	public String getDomain()
	{
		return this.domain;
	}

	/**
	 * Returns whether the cookie is secure connections only.
	 *
	 * @return True if the cookie is for secure connections only.
	 */
	public boolean isSecure()
	{
		return this.secure;
	}

	/**
	 * Returns the cookie expiry date.
	 *
	 * @return The cookie expiry date.
	 */
	public Date getExpires()
	{
		return new Date(this.expires.getTime());
	}

	/**
	 * Sets the cookie name.
	 *
	 * @param name The name of the cookie.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public void setName(String name)
	{
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		this.name = name;
	}

	/**
	 * Sets the cookie value.
	 *
	 * @param value The value of the cookie.
	 * @throws NullPointerException If value is null.
	 * @throws IllegalArgumentException If value is an empty string.
	 */
	public void setValue(String value)
	{
		if(value == null) {
			throw new NullPointerException();
		}
		if(value.isEmpty()) {
			throw new IllegalArgumentException("Missing value");
		}

		this.value = value;
	}

	/**
	 * Sets the cookie comment.
	 *
	 * @param comment The comment of the cookie.
	 * @throws NullPointerException If comment is null.
	 * @throws IllegalArgumentException If comment is an empty string.
	 */
	public void setComment(String comment)
	{
		if(comment == null) {
			throw new NullPointerException();
		}
		if(comment.isEmpty()) {
			throw new IllegalArgumentException("Missing comment");
		}

		this.comment = comment;
	}

	/**
	 * Sets the cookie path.
	 *
	 * @param path The path of the cookie.
	 * @throws NullPointerException If path is null.
	 * @throws IllegalArgumentException If path is an empty string.
	 */
	public void setPath(String path)
	{
		if(path == null) {
			throw new NullPointerException();
		}
		if(path.isEmpty()) {
			throw new IllegalArgumentException("Missing path");
		}

		this.path = path;
	}

	/**
	 * Sets the cookie domain.
	 *
	 * @param domain The domain of the cookie.
	 * @throws NullPointerException If domain is null.
	 * @throws IllegalArgumentException If domain is an empty string.
	 */
	public void setDomain(String domain)
	{
		if(domain == null) {
			throw new NullPointerException();
		}
		if(domain.isEmpty()) {
			throw new IllegalArgumentException("Missing domain");
		}

		this.domain = domain;
	}

	/**
	 * Sets whether the cookie is for secure connections only.
	 *
	 * @param secure True if the cookie is for secure connections only.
	 */
	public void setSecure(boolean secure)
	{
		this.secure = secure;
	}

	/**
	 * Sets the cookie expiry date.
	 *
	 * @param expires The expiry date of the cookie.
	 * @throws NullPointerException If expires is null.
	 */
	public void setExpires(Date expires)
	{
		if(expires == null) {
			throw new NullPointerException();
		}

		this.expires = new Date(expires.getTime());
	}

	/**
	 * Returns a string representation of this cookie which can be sent within a HTTP response.
	 */
	public String toString()
	{
		StringBuffer buffer = new StringBuffer();

		buffer.append(name).append("=").append(value);

		if(this.comment != null) {
			buffer.append("; Comment=").append(this.comment);
		}
		if(this.domain != null) {
			buffer.append("; Domain=").append(this.domain);
		}
		if(this.expires != null) {
			int maxAge = (int)((this.expires.getTime() - (new Date()).getTime()) / 1000);
			buffer.append("; Max-Age=").append((maxAge < 0) ? 0 : maxAge);
		}
		if(this.path != null) {
			buffer.append("; Path=").append(this.path);
		}
		if(this.secure) {
			buffer.append("; Secure");
		}

		buffer.append("; Version=1");

		return buffer.toString();
	}
}
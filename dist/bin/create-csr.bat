@echo off
cd %~DP0/../ssl

IF NOT EXIST mango.jks (
	echo A keystore does not exist.
) else (
	keytool -certreq -alias mango -file server.csr -keystore mango.jks
)

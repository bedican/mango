/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.util.*;

/**
 * A request handler handles the request to produce a response.
 */
public interface RequestHandler
{
	/**
	 * Handles the request/response pair.
	 *
	 * @param request The request object holding information about the request.
	 * @param response The response object providing functionality to send a response to the client.
	 * @throws RequestHandlerException If the request could not be handled by this request handler.
	 * @throws SessionManagerException If there was a problem with creation or retrieval of the session associated with the request.
	 * @throws IOException If an I/O error occurs when handling the request.
	 */
	public void handleRequest(Request request, Response response) throws RequestHandlerException, SessionManagerException, IOException;

	/**
	 * Initializes the request handler. Called at the start of the request handler lifecycle.
	 *
	 * @param context The server context.
	 * @param config The request handler configuration.
	 * @throws Exception If a problem occurs.
	 */
	public void init(ServerContext context, RequestHandlerConfig config) throws RequestHandlerException;

	/**
	 * Called at the end of the request handler lifecycle on server shutdown.
	 */
	public void destroy();
}
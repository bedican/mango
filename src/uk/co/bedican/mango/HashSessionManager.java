/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.util.*;
import java.util.concurrent.*;

/**
 * <p>Store and retrieves sessions within memory.</p>
 * <p>A background monitor thread runs to perform cleanup of invalidated sessions from memory.</p>
 */
public final class HashSessionManager extends AbstractSessionManager
{
	private Map<String, Session> sessions;
	private MonitorThread monitor;

	/**
	 * Creates a new HashSessionManager instance.
	 */
	public HashSessionManager()
	{
		this.sessions = new ConcurrentHashMap<String, Session>();
		this.monitor = new MonitorThread(this);
		this.monitor.start();
	}

	/**
	 * Retrieves a session from memory from a given session ID.
	 * If the session does not exist and the create parameter is true, a new session will be returned, otherwise null.
	 *
	 * @param sessionId The ID of the session to retrieve.
	 * @param create Whether to create a new session if the session for the given ID does not exist.
	 * @return A session for the given ID or if no session is available a new session if the create parameter is true otherwise null.
	 * @throws SessionManagerException If there was a problem with creation or retrieval of the session.
	 */
	protected Session load(String sessionId, boolean create) throws SessionManagerException
	{
		Session session = this.sessions.get(sessionId);

		if((session != null) && (session.isValid())) {
			return session;
		}

		if(session != null) {
			this.delete(session);
		}

		if(!create) {
			return null;
		}

		session = new Session(sessionId);
		this.sessions.put(sessionId, session);

		return session;
	}

	/**
	 * Saves a given session to memory.
	 *
	 * @param session The session to save.
	 * @throws SessionManagerException If there was a problem with saving the session.
	 */
	protected void save(Session session) throws SessionManagerException
	{
		this.sessions.put(session.getId(), session);
	}

	/**
	 * Removes a given session from memory.
	 *
	 * @param session The session to remove.
	 * @throws SessionManagerException If there was a problem with removing the session.
	 */
	protected void delete(Session session) throws SessionManagerException
	{
		this.sessions.remove(session.getId());
	}

	/**
	 * Called on server shutdown.
	 */
	public void shutdown()
	{
		this.monitor.shutdown();
	}

	private void cleanup()
	{
		Collection<Session> sessions = this.sessions.values();

		for(Session session:sessions) {
			if(!session.isValid()) {
				try { this.removeSession(session); } catch(SessionManagerException sme) {}
			}
		}
	}

	private static class MonitorThread extends Thread
	{
		private HashSessionManager manager;
		private boolean invokeShutdown = false;

		public MonitorThread(HashSessionManager manager)
		{
			this.manager = manager;
		}

		public void shutdown()
		{
			this.invokeShutdown = true;
			this.interrupt();
		}

		public void run()
		{
			while(!this.invokeShutdown)
			{
				try {
					Thread.sleep(15 * 60 * 1000);
				} catch(InterruptedException ie) {
					continue;
				}

				this.manager.cleanup();
			}
		}
	}
}
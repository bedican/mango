/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

/**
 * This class represents the HTTP status.
 */
public enum HttpStatus
{
	// 2xx OK
	OK(200, "OK"),
	CREATED(201, "Created"),
	ACCEPTED(202, "Accepted"),
	NON_AUTHORATIVE(203, "Non-Authoritative Information"),
	NO_CONTENT(204, "No Content"),
	RESET_CONTENT(205, "Reset Content"),
	PARTIAL_CONTENT(206, "Partial Content"),

	// 3xx Redirection
	MUTIPLE_CHOICES(300, "Multiple Choices"),
	MOVED_PERMANENTLY(301, "Moved Permanently"),
	FOUND(302, "Found"),
	SEE_OTHER(303, "See Other"),
	NOT_MODIFIED(304, "Not Modified"),
	USE_PROXY(305, "Use Proxy"),
	TEMPORARY_REDIRECT(307, "Temporary Redirect"),

	// 4xx Client Error
	BAD_REQUEST(400, "Bad Request"),
	UNAUTHORIZED(401, "Unauthorized"),
	PAYMENT_REQUIRED(402, "Payment Required"),
	FORBIDDEN(403, "Forbidden"),
	NOT_FOUND(404, "Not Found"),
	METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
	NOT_ACCEPTABLE(406, "Not Acceptable"),
	PROXY_AUTHENTICATION_REQUIRED(407, "Proxy Authentication Required"),
	REQUEST_TIMEOUT(408, "Request Timeout"),
	CONFLICT(409, "Conflict"),
	GONE(410, "Gone"),
	LENGTH_REQUIRED(411, "Length Required"),
	PRECONDITION_FAILED(412, "Precondition Failed"),
	ENTITY_TOO_LARGE(413, "Request Entity Too Large"),
	URI_TOO_LONG(414, "Request-URI Too Long"),
	UNSUPPORTED_MEDIA_TYPE(415, "Unsupported Media Type"),
	RANGE_NOT_SATISFIABLE(416, "Requested Range Not Satisfiable"),
	EXPECTATION_FAILED(417, "Expectation Failed"),

	// 5xx Server Error
	INTERNAL_SERVER_ERROR(500, "Internal Server Error"),
	NOT_IMPLEMENTED(501, "Not Implemented"),
	BAD_GATEWAY(502, "Bad Gateway"),
	SERVICE_UNAVAILBLE(503, "Service Unavailable"),
	GATEWAY_TIMEOUT(504, "Gateway Timeout"),
	VERSION_NOT_SUPPORTED(505, "HTTP Version Not Supported");

	private final int code;
	private final String message;

	HttpStatus(int code, String message)
	{
		this.code = code;
		this.message = message;
	}

	/**
	 * Returns the relevant HttpStatus value for a given status code or null if the value does not exist.
	 *
	 * @param code The status code.
	 */
	public static HttpStatus getHttpStatus(int code)
	{
		for(HttpStatus status:HttpStatus.values()) {
			if(status.getCode() == code) {
				return status;
			}
		}

		return null;
	}

	/**
	 * Returns the status code.
	 *
	 * @return The status code.
	 */
	public int getCode() { return this.code; }

	/**
	 * Returns the status message.
	 *
	 * @return The status message.
	 */
	public String getMessage() { return this.message; }

	/**
	 * Returns whether this HTTP status is a server error.
	 *
	 * @return True if this HTTP status is a server error.
	 */
	public boolean isServerError() { return ((this.code >= 500) && (this.code < 600)); }

	/**
	 * Returns whether this HTTP status is a client error.
	 *
	 * @return True if this HTTP status is a client error.
	 */
	public boolean isClientError() { return ((this.code >= 400) && (this.code < 500)); }

	/**
	 * Returns whether this HTTP status is an error.
	 *
	 * @return True if this HTTP status is an error status.
	 */
	public boolean isError() { return ((this.isClientError()) || (this.isServerError())); }

	/**
	 * Returns a string representing this HTTP status.
	 *
	 * @return A string representing this HTTP status.
	 */
	public String toString() { return String.valueOf(this.code) + " " + this.message; }

}
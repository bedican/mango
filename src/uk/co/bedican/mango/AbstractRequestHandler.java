/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.util.*;

/**
 * Defines common functionality for a RequestHandler.
 */
public abstract class AbstractRequestHandler implements RequestHandler
{
	private ServerContext context = null;
	private RequestHandlerConfig config = null;

	/**
	 * Handles the request/response pair.
	 * This method should be implemented in concrete subclasses as the business logic of the request handler.
	 *
	 * @param request The request object holding information about the request.
	 * @param response The response object providing functionality to send a response to the client.
	 * @throws RequestHandlerException If the request could not be handled by this request handler.
	 * @throws SessionManagerException If there was a problem with creation or retrieval of the session associated with the request.
	 * @throws IOException If an I/O error occurs when handling the request.
	 * @see #handleRequest(Request , Response)
	 */
	protected abstract void doRequest(Request request, Response response) throws RequestHandlerException, SessionManagerException, IOException;

	/**
	 * Called at the start of the request handler lifecycle as the final step of initialization.
	 * This class provides an empty implementation designed to be overriden by concrete subclasses.
	 *
	 * @throws RequestHandlerException If an error occurs during initialization.
	 * @see #init(ServerContext, RequestHandlerConfig)
	 */
	protected void doInit() throws RequestHandlerException
	{
	}

	/**
	 * Called at the end of the request handler lifecycle on server shutdown.
	 * This class provides an empty implementation designed to be overriden by concrete subclasses.
	 */
	public void destroy()
	{
	}

	/**
	 * Returns the server context.
	 *
	 * @return The server context.
	 */
	protected final ServerContext getContext()
	{
		return this.context;
	}

	/**
	 * Returns the server config.
	 *
	 * @return The server config.
	 */
	protected final ServerConfig getServerConfig()
	{
		return this.context.getConfig();
	}

	/**
	 * Returns the specified configuration parameter value for this request handler.
	 *
	 * @param name The configuration parameter name.
	 * @return The requested configuration parameter value.
	 * @throws NullPointerException If name null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	protected final String getInitParameter(String name)
	{
		return this.config.getParameter(name);
	}

	/**
	 * Initializes the request handler. Called at the start of the request handler lifecycle.
	 *
	 * @param context The server context.
	 * @param config The configuration for this request handler.
	 * @throws RequestHandlerException If an error occurs during initialization.
	 * @throws NullPointerException If either context or config is null.
	 * @see #doInit()
	 */
	public final void init(ServerContext context, RequestHandlerConfig config) throws RequestHandlerException
	{
		if((context == null) || (config == null)) {
			throw new NullPointerException();
		}

		this.context = context;
		this.config = new RequestHandlerConfig(config);

		this.doInit();
	}

	/**
	 * The entry point in handling the request/response.
	 *
	 * @param request The request object holding information about the request.
	 * @param response The response object providing functionality to send a response to the client.
	 * @throws RequestHandlerException If the request could not be handled by this request handler.
	 * @throws SessionManagerException If there was a problem with creation or retrieval of the session associated with the request.
	 * @throws IOException If an I/O error occurs when handling the request.
	 * @throws IllegalStateException If the request handler has not been configured by virtue of initialization.
	 * @see #doRequest(Request , Response)
	 */
	public final void handleRequest(Request request, Response response) throws RequestHandlerException, SessionManagerException, IOException
	{
		if(this.config == null) {
			throw new IllegalStateException("Request handler has not been configured");
		}

		this.doRequest(request, response);
	}
}
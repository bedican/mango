/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.util.*;

/**
 * This class represents a request handler configuration node within the server configuration.
 */
public class RequestHandlerConfig
{
	private String name;
	private Class requestHandlerClass;

	private List<String> patterns = new Vector<String>();
	private Map<String, String> parameters = new HashMap<String, String>();

	/**
	 * Creates a new RequestHandlerConfig instance with a given request handler identifier name and request handler class.
	 *
	 * @param name The request handler identifier name.
	 * @param requestHandlerClass The Class of the request handler.
	 * @throws NullPointerException If either name or requestHandlerClass is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public RequestHandlerConfig(String name, Class requestHandlerClass)
	{
		if((name == null) || (requestHandlerClass == null)) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		this.name = name;
		this.requestHandlerClass = requestHandlerClass;
	}

	/**
	 * Creates a new RequestHandlerConfig instance from a given RequestHandlerConfig instance.
	 *
	 * @param config The request handler configuration to copy.
	 * @throws NullPointerException If config is null.
	 */
	public RequestHandlerConfig(RequestHandlerConfig config)
	{
		if(config == null) {
			throw new NullPointerException();
		}

		this.name = config.getName();
		this.requestHandlerClass = config.getRequestHandlerClass();
		this.patterns = new Vector<String>(config.getPatterns());
		this.parameters = new HashMap<String, String>(config.getParameters());
	}

	/**
	 * Returns a new RequestHandler instance with this configuration.
	 *
	 * @param context The server context used to initialize the new request handler.
	 * @return A new RequestHandler instance with this configuration.
	 * @throws NullPointerException If context is null.
	 */
	public RequestHandler newRequestHandler(ServerContext context)
	{
		if(context == null) {
			throw new NullPointerException();
		}

		RequestHandler handler;

		try {
			handler = (RequestHandler)this.requestHandlerClass.newInstance();
			handler.init(context, this);
		} catch(Exception e) {
			throw new RuntimeException("Failed to create request handler: '" + this.name + "' (" + this.requestHandlerClass.getName() + ")", e);
		}

		return handler;
	}

	/**
	 * Returns the identifier name of the request handler.
	 *
	 * @return The identifier name of the request handler.
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Returns the Class of the request handler.
	 *
	 * @return The Class of the request handler.
	 */
	public Class getRequestHandlerClass()
	{
		return this.requestHandlerClass;
	}

	/**
	 * Returns a list of url patterns associated with the request handler.
	 *
	 * @return A list of url patterns associated with the request handler.
	 */
	public List<String> getPatterns()
	{
		return new Vector<String>(this.patterns);
	}

	/**
	 * Adds an url pattern.
	 * An url pattern is specified as a requested path where * may match any character excluding a forward slash and ** may match any character including a forward slash.
	 *
	 * @param pattern The url pattern.
	 * @throws NullPointerException If pattern is null.
	 * @throws IllegalArgumentException If pattern is an empty string.
	 */
	public void addPattern(String pattern)
	{
		if(pattern == null) {
			throw new NullPointerException();
		}
		if(pattern.isEmpty()) {
			throw new IllegalArgumentException("Missing pattern");
		}

		this.patterns.add(pattern);
	}

	/**
	 * Returns the parameters of this request handler configuration.
	 *
	 * @return The parameters of this request handler configuration.
	 */
	public Map<String, String> getParameters()
	{
		return new HashMap<String, String>(this.parameters);
	}

	/**
	 * Returns whether a parameter value for the given name exists.
	 *
	 * @param name The parameter name.
	 * @return True if a parameter value for the given name exists.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public boolean hasParameter(String name)
	{
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		 return this.parameters.containsKey(name);
	}

	/**
	 * Returns the parameter value for the given name.
	 *
	 * @param name The parameter name.
	 * @return The parameter value for the given name.
	 * @throws NullPointerException If name is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public String getParameter(String name)
	{
		if(name == null) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		 return this.parameters.get(name);
	}

	/**
	 * Sets the parameter value for the given name.
	 *
	 * @param name The parameter name.
	 * @param value The parameter value.
	 * @throws NullPointerException If either name or value is null.
	 * @throws IllegalArgumentException If name is an empty string.
	 */
	public void setParameter(String name, String value)
	{
		if((name == null) || (value == null)) {
			throw new NullPointerException();
		}
		if(name.isEmpty()) {
			throw new IllegalArgumentException("Missing name");
		}

		this.parameters.put(name, value);
	}
}
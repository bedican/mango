/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.regex.*;

/**
 * <p>Proxies requests to an alternative server.</p>
 * <p>The following configuration options are available:</p>
 * <ul>
 * <li>hostname - Origin server hostname to forward requests.</li>
 * <li>port - Origin server port, defaults to 80.</li>
 * <li>proxyPath - Base path for proxy server. If the proxyPath is /foo/bar and the request path is /foo/bar/directory/file a request for /directory/file is made to the origin server.</li>
 * <li>originPath - Base path for origin server. If the originPath is /foo/bar and the request path is /directory/file a request for /foo/bar/directory/file is made to the origin server.</li>
 * </ul>
 * <p>For example:</p>
 * <p>
 * If proxyPath is /proxy and originPath is /origin a request of /proxy/my/path will result in a request of /origin/my/path being made to the origin server.
 * This is to say that the proxyPath is subtracted from the request path and then the originPath is prefixed.
 * </p>
 */
public class ProxyRequestHandler extends AbstractRequestHandler
{
	private String hostname;
	private String proxyPath;
	private String originPath;

	private int port = 80;

	/**
	 * Creates a new ProxyRequestHandler instance.
	 */
	public ProxyRequestHandler()
	{
	}

	/**
	 * Initializes the request handler from request handler configuration.
	 *
	 * @throws RequestHandlerException If an error occurs during initialization.
	 */
	protected void doInit() throws RequestHandlerException
	{
		this.hostname = this.getInitParameter("hostname");
		if(this.hostname != null) {
			this.hostname = this.hostname.trim().toLowerCase();
		}
		if((this.hostname == null) || (this.hostname.isEmpty())) {
			throw new RequestHandlerException("Missing or empty hostname configuration");
		}

		String port = this.getInitParameter("port");
		if(port != null)
		{
			try {
				this.port = Integer.parseInt(port.trim());
			} catch(NumberFormatException nfe) {
				throw new RequestHandlerException("Invalid port configuration specified (" + port + ")", nfe);
			}
		}

		this.proxyPath = this.getInitParameter("proxyPath");
		this.proxyPath = (this.proxyPath != null) ? this.proxyPath.trim() : "";

		this.originPath = this.getInitParameter("originPath");
		this.originPath = (this.originPath != null) ? this.originPath.trim() : "";
	}

	/**
	 * Proxies request to the configured origin server. Location headers will be translated, however content (for example HTML hrefs etc) will not.
	 *
	 * @param request The request object holding information about the request.
	 * @param response The response object providing functionality to send a response to the client.
	 * @throws RequestHandlerException If the request could not be handled by this request handler.
	 * @throws SessionManagerException If there was a problem with creation or retrieval of the session associated with the request.
	 * @throws IOException If an I/O error occurs when handling the request.
	 */
	protected void doRequest(Request request, Response response) throws RequestHandlerException, SessionManagerException, IOException
	{
		Socket socket = new Socket(this.hostname, this.port);
		socket.setSoTimeout(8000);

		BufferedInputStream in = new BufferedInputStream(socket.getInputStream());
		PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

		// Build new path

		String path = request.getRequestedPath();

		// Subtract this.proxyPath
		if(!this.proxyPath.isEmpty()) {
			if(path.substring(0, this.proxyPath.length()).equals(this.proxyPath)) {
				path = path.substring(this.proxyPath.length());
			}
		}

		// Prefix with this.originPath
		if(!this.originPath.isEmpty()) {
			path = this.originPath + path;
		}

		// Send request

		out.println(request.getMethod() + " " + path + " HTTP/" + request.getHttpVersion());
		out.println("Host: " + this.hostname);
		out.println("X-Forwarded-For: " + request.getInetAddress().getHostAddress());
		out.println("Via: " + request.getHttpVersion() + " " + InetAddress.getLocalHost().getHostName() + " (" + Server.VERSION + ")");
		out.println("Connection: close");

		List<String> headerNames = request.getHeaderNames();
		for(String headerName:headerNames)
		{
			if(
			(!headerName.toLowerCase().equals("host")) && 
			(!headerName.toLowerCase().equals("x-forwarded-for")) &&
			(!headerName.toLowerCase().equals("connection")) &&
			(!headerName.toLowerCase().equals("keep-alive")) &&
			(!headerName.toLowerCase().equals("via"))
			) {
				out.println(headerName + ": " + request.getHeader(headerName));
			}
		}

		StringBuffer cookieHeader = new StringBuffer();
		List<String> cookieNames = request.getCookieNames();
		for(String cookieName:cookieNames) {
			cookieHeader.append("; ").append(cookieName).append("=").append(request.getCookie(cookieName).getValue());
		}
		if(cookieHeader.length() != 0) {
			out.println("Cookie: " + cookieHeader.toString().substring(2));
		}

		out.println("");

		if((request.getMethod().equals("POST")) && (!request.getRawPostData().isEmpty())) {
			out.println(request.getRawPostData());
		}

		// Parse response

		char ch;
		String headersString;
		StringBuilder headersBuilder = new StringBuilder();

		while((ch = (char)in.read()) != -1)
		{
			headersBuilder.append(ch);
			headersString = headersBuilder.toString();

			if((headersString.endsWith("\n\n")) || (headersString.endsWith("\r\n\r\n"))) {
				break;
			}
		}

		response.clearHeaders();
		response.setHeader("Connection", "close");

		BufferedReader headers = new BufferedReader(new StringReader(headersBuilder.toString()));

		String line;
		Matcher matcher;

		Pattern statusPattern = Pattern.compile("HTTP/1\\.[01]\\s([0-9]+)");

		line = headers.readLine();
		if(line == null) {
			throw new RequestHandlerException("Received malformed response");
		}

		matcher = statusPattern.matcher(line);
		if(!matcher.lookingAt()) {
			throw new RequestHandlerException("Received malformed status line");
		}

		response.setStatus(HttpStatus.getHttpStatus(Integer.parseInt(matcher.group(1))));

		Pattern headerPattern = Pattern.compile("([^:]+):\\s*(.+)");

		while(((line = headers.readLine()) != null) && (!line.isEmpty()))
		{
			matcher = headerPattern.matcher(line);
			if((matcher.matches()) && (!matcher.group(1).trim().toLowerCase().equals("connection"))) {
				response.addHeader(matcher.group(1).trim(), matcher.group(2).trim());
			}
		}

		if(response.hasHeader("Location"))
		{
			URL location = new URL(response.getHeader("Location").get(0));
			String locationPath = location.getPath();

			// Subtract this.originPath
			if(!this.originPath.isEmpty()) {
				if(locationPath.substring(0, this.originPath.length()).equals(this.originPath)) {
					locationPath = locationPath.substring(this.originPath.length());
				}
			}

			// Prefix with this.proxyPath
			if(!this.proxyPath.isEmpty()) {
				locationPath = this.proxyPath + locationPath;
			}

			response.setHeader("Location", location.toString().replace(location.getPath(), locationPath));
		}

		int bytesRead;
		byte[] buffer = new byte[4096];

		while((bytesRead = in.read(buffer, 0, 4096)) != -1)
		{
			response.getOutputStream().write(buffer, 0, bytesRead);
			response.flushBuffer();
		}

		socket.close();
	}
}
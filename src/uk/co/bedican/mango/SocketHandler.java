/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.net.*;

/**
 * Turns sockets into request/response pairs and passes control to request dispatchers.
 */
public class SocketHandler implements Runnable
{
	private Socket socket;
	private ServerContext context;

	/**
	 * Creates a new SocketHandler instance with the given server context and socket.
	 *
	 * @param context The server context.
	 * @param socket The socket to handle.
	 * @throws NullPointerException If either context or socket is null.
	 */
	public SocketHandler(ServerContext context, Socket socket)
	{
		if((context == null) || (socket == null)) {
			throw new NullPointerException();
		}

		this.context = context;
		this.socket = socket;
	}

	private void close()
	{
		try
		{
			this.socket.close();
		}
		catch(IOException ioe)
		{
		}
	}

	/**
	 * Handles the encapsulated socket by processing the request data from its input stream to produce a response on its output stream.
	 */
	public void run()
	{
		try
		{
			this.socket.setSoTimeout(4000);

			Request request = new Request(this.socket.getInetAddress(), this.socket.getInputStream());
			Response response = new Response(this.socket.getOutputStream());

			response.setContext(this.context);
			response.setRequest(request);
			response.initHeaders();

			request.setSessionManager(this.context.getSessionManager());

			this.context.getSessionManager().addListener(response);

			try
			{
				request.parseRequest();
				//this.socket.shutdownInput();

				if(request.isMethodHead()) {
					response.sendHeadersOnly();
				}

				// We could do a filter chain here...
				this.context.getRequestDispatcher(request.getPath()).forward(request, response);

				Session session = request.getSession(false);

				if(session != null)
				{
					if(session.isValid()) {
						this.context.getSessionManager().putSession(session);
					} else {
						this.context.getSessionManager().removeSession(session);
					}
				}
			}
			catch(RequestException re)
			{
				response.sendError(re.getHttpStatus());
			}
			catch(Throwable throwable)
			{
				if(!response.isCommited()) {
					response.sendThrowable(throwable);
				}

				this.context.getErrorLogger().log(throwable);
			}
			finally
			{
				this.context.getSessionManager().removeListener(response);
				this.context.getRequestLogger().log(request, response);

				//this.socket.shutdownOutput();
			}
		}
		catch(Throwable throwable)
		{
			this.context.getErrorLogger().log(throwable);
			throwable.printStackTrace();
		}
		finally
		{
			this.close();
		}
	}
}
/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

/**
 * Signals an error has occured in reading the request.
 */
public class RequestException extends Exception
{
	private HttpStatus status;

	/**
	 * Constructs a new RequestException with the specified HTTP status.
	 * The detail message is set with the HTTP status description.
	 *
	 * @param status The HTTP status.
	 */
	public RequestException(HttpStatus status)
	{
		super(status.getMessage());
		this.status = status;
	}

	/**
	 * Constructs a new RequestException with the specified HTTP status and detail message.
	 *
	 * @param status The HTTP status.
	 * @param message The detail message.
	 */
	public RequestException(HttpStatus status, String message)
	{
		super(message);
		this.status = status;
	}

	/**
	 * Returns the HTTP status of this request exception.
	 *
	 * @return The HTTP status of this request exception.
	 */
	public HttpStatus getHttpStatus()
	{
		return this.status;
	}
}
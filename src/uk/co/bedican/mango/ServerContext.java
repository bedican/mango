/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.util.*;
import java.util.regex.*;

/**
 * A server context provides the configured services used by the server throughout its life.
 */
public class ServerContext
{
	private ServerConfig config;

	private RequestLogger requestLogger;
	private ErrorLogger errorLogger;

	private String defaultMimeType = "text/plain";

	private SessionManager sessionManager;

	private RequestHandler defaultHandler, errorHandler;
	private Map<Pattern, RequestHandler> handlers;

	/**
	 * Creates a new ServerContext instance with a given server configuration.
	 *
	 * @param config The server configuration.
	 * @throws NullPointerException If config is null.
	 */
	public ServerContext(ServerConfig config)
	{
		if(config == null) {
			throw new NullPointerException();
		}

		this.config = config;

		this.initLoggers();
		this.initSessionManager();
		this.initRequestHandlers();
		this.initDefaultRequestHandler();
		this.initErrorRequestHandler();
	}

	/**
	 * Returns the server config.
	 *
	 * @return The server config.
	 */
	public ServerConfig getConfig()
	{
		return this.config;
	}

	/**
	 * Returns the request logger.
	 *
	 * @return The request logger.
	 */
	public RequestLogger getRequestLogger()
	{
		return this.requestLogger;
	}

	/**
	 * Returns the error logger.
	 *
	 * @return The error logger.
	 */
	public ErrorLogger getErrorLogger()
	{
		return this.errorLogger;
	}

	/**
	 * Called on server shutdown.
	 * Shuts down the session manager and calls destroy on all request handlers.
	 *
	 * @see SessionManager#shutdown()
	 * @see RequestHandler#destroy()
	 */
	public void shutdown()
	{
		this.sessionManager.shutdown();
		this.defaultHandler.destroy();

		Collection<RequestHandler> handlers = this.handlers.values();
		for(RequestHandler handler:handlers) {
			handler.destroy();
		}
	}

	private void initLoggers()
	{
		File directory;

		directory = this.config.getRequestLogFile().getParentFile();
		if(!directory.exists()) {
			directory.mkdirs(); // TODO: throw IOException on failure
		}

		directory = this.config.getErrorLogFile().getParentFile();
		if(!directory.exists()) {
			directory.mkdirs(); // TODO: throw IOException on failure
		}

		this.requestLogger = new RequestLogger(this.config.getRequestLogFile());
		this.errorLogger = new ErrorLogger(this.config.getErrorLogFile());
	}
	private void initSessionManager()
	{
		this.sessionManager = null;

		Class sessionManagerClass = this.config.getSessionManager();
		if(sessionManagerClass != null)
		{
			try {
				this.sessionManager = (SessionManager)sessionManagerClass.newInstance();
			} catch(Exception e) {
			}
		}

		if(this.sessionManager == null) {
			this.sessionManager = new DefaultSessionManager();
		}
	}
	private void initRequestHandlers()
	{
		this.handlers = new HashMap<Pattern, RequestHandler>();

		Map<String, RequestHandlerConfig> requestHandlerConfigs = this.config.getRequestHandlerConfigs();
		Set<Map.Entry<String, RequestHandlerConfig>> entries = requestHandlerConfigs.entrySet();

		List<String> patterns;
		RequestHandler handler;
		RequestHandlerConfig config;

		for(Map.Entry<String, RequestHandlerConfig> entry:entries)
		{
			config = entry.getValue();
			patterns = config.getPatterns();

			if(patterns.isEmpty()) {
				continue;
			}

			handler = config.newRequestHandler(this);

			for(String pattern:patterns)
			{
				pattern = "\\Q" + pattern + "\\E";
				pattern = pattern.replace("**", "\\E.+?\\Q");
				pattern = pattern.replace("*", "\\E[^/]+\\Q");
				pattern = pattern.replace("\\Q\\E", "");

				this.handlers.put(Pattern.compile(pattern), handler);
			}
		}
	}
	private void initDefaultRequestHandler()
	{
		this.defaultHandler = this.config.getDefaultRequestHandlerConfig().newRequestHandler(this);
	}
	private void initErrorRequestHandler()
	{
		this.errorHandler = this.config.getErrorRequestHandlerConfig().newRequestHandler(this);
	}

	/**
	 * <p>Returns the request dispatcher encapsulating the request handler used to handle the error document for the given HTTP status.</p>
	 * <p>If no error document is configured for the given HTTP status, a request dispatcher encapsulating the error request handler is returned.</p>
	 *
	 * @param status The HTTP status.
	 * @return The request dispatcher for the given HTTP status.
	 * @throws NullPointerException If status is null.
	 * @see #getErrorDocumentDispatcher()
	 */
	public RequestDispatcher getErrorDocumentDispatcher(HttpStatus status)
	{
		if(status == null) {
			throw new NullPointerException();
		}

		Integer code = Integer.valueOf(status.getCode());
		Map<Integer, String> errorDocuments = this.config.getErrorDocuments();

		if(errorDocuments.containsKey(code)) {
			return this.getRequestDispatcher(errorDocuments.get(code));
		}

		return this.getErrorDocumentDispatcher();
	}

	/**
	 * Returns a request dispatcher encapsulating the error request handler.
	 *
	 * @return A request dispatcher encapsulating the error request handler.
	 */
	public RequestDispatcher getErrorDocumentDispatcher()
	{
		return new RequestDispatcher(this.errorHandler);
	}

	/**
	 * Returns the request dispatcher encapsulating the request handler used to handle requests for the given path.
	 *
	 * @param path The request path relative to the document root.
	 * @return The request dispatcher for the given path.
	 * @throws NullPointerException If path is null.
	 * @throws IllegalArgumentException If path is an empty string.
	 */
	public RequestDispatcher getRequestDispatcher(String path)
	{
		return new RequestDispatcher(this.getRequestHandler(path), path);
	}
	private RequestHandler getRequestHandler(String path)
	{
		if(path == null) {
			throw new NullPointerException();
		}
		if(path.isEmpty()) {
			throw new IllegalArgumentException("Missing path");
		}

		Matcher matcher;
		Set<Pattern> patterns = this.handlers.keySet();

		for(Pattern pattern:patterns) {
			if(pattern.matcher(path).matches()) {
				return this.handlers.get(pattern);
			}
		}

		return this.defaultHandler;
	}

	/**
	 * Returns the session manager.
	 *
	 * @return The session manager.
	 */
	public SessionManager getSessionManager()
	{
		return this.sessionManager;
	}

	/**
	 * Returns the MIME type for the given file. If no MIME type is configured, the default MIME type &quot;text/plain&quot; is returned.
	 *
	 * @param file The file.
	 * @return The MIME type for the given file.
	 * @throws NullPointerException If file is null.
	 */
	public String getMimeType(File file)
	{
		if(file == null) {
			throw new NullPointerException();
		}

		Map<String, String> mimeTypes = this.config.getMimeTypes();

		String filename = file.getAbsolutePath().toLowerCase();
		Set<String> keys = mimeTypes.keySet();

		for(String extension:keys) {
			if(filename.endsWith("." + extension)) {
				return mimeTypes.get(extension);
			}
		}

		return this.defaultMimeType;
	}
}
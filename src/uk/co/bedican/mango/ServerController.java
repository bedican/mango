/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.net.*;

/**
 * A server controller controls a Server instance and listens on a local port for control requests.
 * The port is specified within the server configuration as the controller port.
 */
public class ServerController extends Thread
{
	private Server server;
	private ServerControllerClient client = null;

	/**
	 * Creates a new ServerController instance with a given server to control.
	 *
	 * @param server The server to control.
	 * @throws NullPointerException If server is null.
	 */
	public ServerController(Server server)
	{
		if(server == null) {
			throw new NullPointerException();
		}

		this.server = server;
	}

	/**
	 * Returns a server controller client to send control requests to the listening controller.
	 *
	 * @return A server controller client to send control requests.
	 */
	public ServerControllerClient getClient()
	{
		if(this.client == null) {
			this.client = new ServerControllerClient(this.server.getConfig().getControllerPort());
		}

		return this.client;
	}

	/**
	 * <p>Starts the encapsulated server and listens for local control requests on the configured port.</p>
	 * <p>This method should not be called directly. Instead use start() to ensure this controller runs within a seperate Thread.</p>
	 *
	 * @see #start()
	 */
	public void run()
	{
		String line = null;
		BufferedReader in;
		PrintWriter out;
		Socket socket;

		this.server.start();

		try
		{
			ServerSocket serverSocket = new ServerSocket(this.server.getConfig().getControllerPort(), 1, InetAddress.getByName("localhost"));

			do
			{
				try
				{
					socket = serverSocket.accept();
					socket.setSoTimeout(4000);

					if(socket.getInetAddress().isLoopbackAddress())
					{
						in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
						out = new PrintWriter(socket.getOutputStream(), true);

						line = in.readLine();

						if((line != null) && (line.equals("STOP")))
						{
							out.println("Server shutting down ...");
							this.server.shutdown();
						}
					}

					socket.close();
				}
				catch(IOException ioe)
				{
					try { Thread.sleep(500); } catch(InterruptedException ie) {}
				}
			}
			while((line != null) && (!line.equals("STOP")));
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}

	/**
	 * A server controller client is used to send control requests locally to the listening controller.
	 */
	public static class ServerControllerClient
	{
		private int port;

		/**
		 * Creates a new ServerControllerClient instance with a given port.
		 *
		 * @param port The port to send control requests.
		 */
		public ServerControllerClient(int port)
		{
			this.port = port;
		}

		/**
		 * Sends a control request to the locally listening controller.
		 *
		 * @param command The command to send.
		 * @return The controller response.
		 * @throws NullPointerException if command is null.
		 * @throws IllegalArgumentException if command is an empty string.
		 */
		public String send(String command)
		{
			if(command == null) {
				throw new NullPointerException();
			}
			if(command.isEmpty()) {
				throw new IllegalArgumentException("Empty command");
			}

			String result = "";

			try
			{
				Socket socket = new Socket("127.0.0.1", this.port);
				socket.setSoTimeout(8000);

				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

				out.println(command.toUpperCase());

				result = in.readLine();

				socket.close();
			}
			catch(IOException ioe)
			{
				return ioe.getMessage();
			}

			return result;
		}
	}
}
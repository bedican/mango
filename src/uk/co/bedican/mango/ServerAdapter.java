/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

/**
 * <p>An abstract adapter class for receiving server events. The methods in this class are empty. This class exists as convenience for creating listener objects.</p>
 * <p>Extend this class to create a ServerListener and override the methods for the events of interest.</p>
 *
 * @see ServerListener
 */
public abstract class ServerAdapter implements ServerListener
{
	/**
	 * Invoked when a Server is started.
	 *
	 * @param server The server which has been started.
	 */
	public void serverStarted(Server server) {}

	/**
	 * Invoked when a Server has been requested to stop.
	 *
	 * @param server The server which has been requested to stop.
	 */
	public void serverStopping(Server server) {}

	/**
	 * Invoked when a Server is stopped.
	 *
	 * @param server The server which has been stopped.
	 */
	public void serverStopped(Server server) {}
}
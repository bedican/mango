@echo off
cd %~DP0/../ssl

IF NOT EXIST mango.jks (
	echo A keystore does not exist.
) else (
	keytool -import -trustcacerts -alias mango -file server.crt -keystore mango.jks
)

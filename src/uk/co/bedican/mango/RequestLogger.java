/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;

/**
 * This class represents a server request log.
 */
public class RequestLogger extends AbstractLogger
{
	/**
	 * Creates a new RequestLogger instance.
	 * 
	 * @param file The File representing the underlying request log file.
	 * @throws NullPointerException if file is null.
	 */
	public RequestLogger(File file)
	{
		this.init(file);
	}

	/**
	 * Logs a request entry within the request log for a given request and response pair.
	 * 
	 * @param request The request object.
	 * @param response The response object.
	 * @throws NullPointerException if either request or response is null.
	 */
	public void log(Request request, Response response)
	{
		if((request == null) || (response == null)) {
			throw new NullPointerException();
		}

		StringBuffer buffer = new StringBuffer();

		buffer.append(this.formatDate(request.getDate()));
		buffer.append(" ");
		buffer.append(request.getInetAddress().getHostAddress());
		buffer.append(" \"").append(request.getFirstLine()).append("\" ");
		buffer.append(response.getStatus().getCode());
		buffer.append(" ");
		buffer.append(response.getBytesWritten());
		buffer.append(" \"").append(request.hasHeader("referer") ? request.getHeader("referer") : "-").append("\"");
		buffer.append(" \"").append(request.hasHeader("user-agent") ? request.getHeader("user-agent") : "-").append("\"");

		this.getPrintStream().println(buffer.toString());
	}
}

/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

/**
 * <p>An abstract adapter class for receiving session events. The methods in this class are empty. This class exists as convenience for creating listener objects.</p>
 * <p>Extend this class to create a SessionListener and override the methods for the events of interest.</p>
 *
 * @see SessionListener
 */
public abstract class SessionAdapter implements SessionListener
{
	/**
	 * Invoked when a session is created.
	 *
	 * @param session The session which has been created.
	 */
	public void sessionCreated(Session session) {}

	/**
	 * Invoked when a session is destroyed.
	 *
	 * @param session The session which has been destroyed.
	 */
	public void sessionDestroyed(Session session) {}
}
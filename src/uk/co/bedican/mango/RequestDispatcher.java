/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.net.*;
import java.io.*;

/**
 * A request dispatcher encapsulates a request handler and is used to either forward control or include its response.
 */
public class RequestDispatcher
{
	private String path;
	private RequestHandler handler;

	/**
	 * Creates a new RequestDispatcher instance with a given request handler.
	 *
	 * @param handler The request handler.
	 * @throws NullPointerException If handler is null.
	 */
	public RequestDispatcher(RequestHandler handler)
	{
		if(handler == null) {
			throw new NullPointerException();
		}

		this.path = null;
		this.handler = handler;
	}

	/**
	 * Creates a new RequestDispatcher instance with a given request handler and request path.
	 *
	 * @param handler The request handler.
	 * @param path The request path.
	 * @throws NullPointerException If either handler or path is null.
	 * @throws IllegalArgumentException If path is an empty string.
	 */
	public RequestDispatcher(RequestHandler handler, String path)
	{
		if((handler == null) || (path == null)) {
			throw new NullPointerException();
		}
		if(path.isEmpty()) {
			throw new IllegalArgumentException("Missing path");
		}

		this.path = path;
		this.handler = handler;		
	}

	private void modifyRequestPath(Request request) throws RequestHandlerException
	{
		if(this.path == null) {
			return;
		}
		
		request.setPath(this.path);
	}

	/**
	 * Forwards control to the underlying RequestHandler.
	 * Any response previously written is cleared and if set, the Request objects request path is modified with the path of this request dispatcher.
	 *
	 * @param request The request object.
	 * @param response The response object.
	 * @throws RequestHandlerException If the request could not be handled by the underlying request handler.
	 * @throws SessionManagerException If there was a problem with creation or retrieval of the session associated with the request.
	 * @throws IOException If an I/O error occurs when handling the request.
	 */
	public void forward(Request request, Response response) throws RequestHandlerException, SessionManagerException, IOException
	{
		this.modifyRequestPath(request);

		response.clearBuffer();

		this.handler.handleRequest(request, response);

		response.flushBuffer();
		response.close();
	}

	/**
	 * Includes the response of the underlying RequestHandler.
	 * If set, the Request objects request path is modified with the path of this request dispatcher.
	 *
	 * @param request The request object.
	 * @param response The response object.
	 * @throws RequestHandlerException If the request could not be handled by the underlying request handler.
	 * @throws SessionManagerException If there was a problem with creation or retrieval of the session associated with the request.
	 * @throws IOException If an I/O error occurs when handling the request.
	 */
	public void include(Request request, Response response) throws RequestHandlerException, SessionManagerException, IOException
	{
		this.modifyRequestPath(request);
		this.handler.handleRequest(request, response);
	}
}
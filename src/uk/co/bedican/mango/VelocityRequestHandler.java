/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.util.*;

import org.apache.velocity.*;
import org.apache.velocity.app.*;

/**
 * Processes Apache Velocity templates.
 * This request handler builds on the functionality of the DefaultRequestHandler to process the requested file as an Apache Velocity template instead of sending it directly to the client.
 *
 * @see <a href="http://velocity.apache.org">http://velocity.apache.org</a>
 */
public class VelocityRequestHandler extends DefaultRequestHandler
{
	private static final Map<String, String> defaultPageContext = Collections.unmodifiableMap(new HashMap<String, String>());

	private VelocityEngine velocity;

	/**
	 * Creates a new VelocityRequestHandler instance.
	 */
	public VelocityRequestHandler()
	{
	}

	/**
	 * Initializes the request handler and the Velocity engine.
	 *
	 * @throws RequestHandlerException If an error occurs during initialization.
	 */
	protected void doInit() throws RequestHandlerException
	{
		super.doInit();

		try
		{
			this.velocity = new VelocityEngine();
			this.velocity.setProperty("file.resource.loader.path", this.getServerConfig().getDocroot().getPath());
			this.velocity.init();
		}
		catch(Exception e)
		{
			throw new RequestHandlerException("Failed to initialize velocity", e);
		}
	}

	/**
	 * Builds the page context. A page context is a Map given to the Velocity template and is unique per request.
	 * This class provides a default implementation returning an empty Map designed to be overriden by subclasses.
	 *
	 * @param request The Request object.
	 * @param response The Response object.
	 * @return A Map of String against String keys.
	 * @throws IOException If an I/O error occurs.
	 * @see #writeFile(Request, Response)
	 */
	protected Map<String, String> getPageContext(Request request, Response response) throws IOException
	{
		return defaultPageContext;
	}

	/**
	 * <p>Processes a file specified within the request as a Velocity template and writes the result to the response.</p>
	 * <p>The following variables are passed to the template:</p>
	 * <ul>
	 * <li>request - The Request object.</li>
	 * <li>response - The Response object.</li>
	 * <li>session - The Session associated with this request, or null if not present.</li>
	 * <li>context - The ServerContext object.</li>
	 * <li>page - The page context, as returned from this.getPageContext(Request, Response).</li>
	 * </ul>
	 *
	 * @param request The request object holding information about the request, specifically the requested file.
	 * @param response The response object providing functionality to send a response to the client.
	 * @throws IOException If an I/O error occurs when processing the Velocity template.
	 * @see #getPageContext(Request, Response)
	 */
	protected void writeFile(Request request, Response response) throws IOException
	{
		try
		{
			VelocityContext context = new VelocityContext();

			context.put("request", request);
			context.put("response", response);	
			context.put("session", request.getSession(false));
			context.put("context", this.getContext());
			context.put("page", this.getPageContext(request, response));

			this.velocity.getTemplate(request.getFilePath()).merge(context, response.getWriter());
		}
		catch(Exception e)
		{
			throw new IOException("Failed to process velocity template", e);
		}
	}
}
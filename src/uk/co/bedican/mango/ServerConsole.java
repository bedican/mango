/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.util.*;

/**
 * Provides a console interface to starting and stopping the server.
 */
public class ServerConsole
{
	private static void printVersion()
	{
		System.out.println("*********************************************");
		System.out.println(" " + Server.VERSION);
		System.out.println(" http://www.bedican.co.uk");
		System.out.println(" (c) Copyright 2009, " + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)) + " Bedican Solutions");
		System.out.println("*********************************************");
		System.out.println();
	}

	/**
	 * This method should be invoked via the BootLoader in order to make use of ServerClassLoader.
	 *
	 * @see BootLoader
	 * @see ServerClassLoader
	 */
	public static void main(String[] args)
	{
		ServerConsole.printVersion();
		System.out.println("Loading configuration.");

		ServerConfig config = null;
		ServerConfigBuilder configBuilder = null;

		try {
			configBuilder = new ServerConfigBuilder();
			config = configBuilder.build("etc/conf.xml");
		} catch(Exception e) {
			System.out.println(e.getMessage());
			System.exit(1);
		}

		Server server = new Server(config);
		ServerController controller = new ServerController(server);

		server.addListener(new ServerListener()
		{
			public void serverStarted(Server server) {
				System.out.println("Server started.");
			}
			public void serverStopping(Server server) {
				System.out.println("Server shutting down ...");
			}
			public void serverStopped(Server server) {
				System.out.println("Server stopped.");
			}
		});

		if(args.length == 1) {
			System.out.println(controller.getClient().send(args[0]));
		} else {
			controller.start();
		}
	}
}
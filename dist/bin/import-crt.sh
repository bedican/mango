#!/bin/bash

DIR=`dirname $0`
cd $DIR/../ssl

if [ ! -f mango.jks ]; then
	echo A keystore does not exist.
else
	keytool -import -trustcacerts -alias mango -file server.crt -keystore mango.jks
fi

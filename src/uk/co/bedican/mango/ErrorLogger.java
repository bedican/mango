/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.util.*;

/**
 * This class represents a server error log.
 */
public class ErrorLogger extends AbstractLogger
{
	/**
	 * Creates a new ErrorLogger instance.
	 * 
	 * @param file The File representing the underlying error log file.
	 * @throws NullPointerException if file is null.
	 */
	public ErrorLogger(File file)
	{
		this.init(file);
	}

	/**
	 * Logs an error entry within the error log for a given request and response pair.
	 * 
	 * @param request The request object.
	 * @param response The response object.
	 * @throws NullPointerException if either request or response is null.
	 */
	public void log(Request request, Response response)
	{
		if((request == null) || (response == null)) {
			throw new NullPointerException();
		}

		StringBuffer buffer = new StringBuffer();

		buffer.append(this.formatDate(request.getDate()));
		buffer.append(" [");
		buffer.append(response.getStatus().toString());
		buffer.append("] ");
		buffer.append(request.getInetAddress().getHostAddress());

		if(response.getStatus().equals(HttpStatus.NOT_FOUND))
		{
			buffer.append(" File does not exist: ");
			buffer.append(request.getRequestedPath());
			if(request.hasHeader("referer")) {
				buffer.append(", referer: ");
				buffer.append(request.getHeader("referer"));
			}
		}
		else if((response.getStatus().equals(HttpStatus.INTERNAL_SERVER_ERROR)) && (request.hasAttribute("error.throwable")))
		{
			try
			{
				Throwable throwable = (Throwable)request.getAttribute("error.throwable");
				buffer.append(" ").append(this.getStackTrace(throwable));
			}
			catch(ClassCastException cce)
			{
			}
		}

		this.getPrintStream().println(buffer.toString());
	}

	/**
	 * Logs an error entry within the error log for a given Throwable.
	 * 
	 * @param throwable A throwable object.
	 * @throws NullPointerException if throwable is null.
	 */
	public void log(Throwable throwable)
	{
		if(throwable == null) {
			throw new NullPointerException();
		}

		StringBuffer buffer = new StringBuffer();

		buffer.append(this.formatDate(new Date()));
		buffer.append(" [exception] ");
		buffer.append(this.getStackTrace(throwable));
		
		this.getPrintStream().println(buffer.toString());
	}

	private String getStackTrace(Throwable throwable)
	{
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter, true);

		do
		{
			throwable.printStackTrace(printWriter);
			throwable = throwable.getCause();
		}
		while(throwable != null);

		return stringWriter.toString();
	}
}

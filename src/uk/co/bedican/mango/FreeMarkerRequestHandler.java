/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.io.*;
import java.util.*;

import freemarker.template.*;

/**
 * Processes FreeMarker templates.
 * This request handler builds on the functionality of the DefaultRequestHandler to process the requested file as a FreeMarker template instead of sending it directly to the client.
 *
 * @see <a href="http://freemarker.org">http://freemarker.org</a>
 */
public class FreeMarkerRequestHandler extends DefaultRequestHandler
{
	private static final Map<String, Object> defaultPageContext = Collections.unmodifiableMap(new HashMap<String, Object>());

	private Configuration config;

	/**
	 * Creates a new FreeMarkerRequestHandler instance.
	 */
	public FreeMarkerRequestHandler()
	{
	}

	/**
	 * Initializes the request handler and the FreeMarker engine.
	 *
	 * @throws RequestHandlerException If an error occurs during initialization.
	 */
	protected void doInit() throws RequestHandlerException
	{
		super.doInit();

		try
		{
			this.config = new Configuration();
			this.config.setDirectoryForTemplateLoading(this.getServerConfig().getDocroot());
			this.config.setObjectWrapper(new DefaultObjectWrapper());
		}
		catch(IOException ioe)
		{
			throw new RequestHandlerException("Failed to initialize free marker", ioe);
		}
	}

	/**
	 * Builds the page context. A page context is a Map given to the FreeMarker template and is unique per request.
	 * This class provides a default implementation returning an empty Map designed to be overriden by subclasses.
	 *
	 * @param request The Request object.
	 * @param response The Response object.
	 * @return A Map of Object against String keys.
	 * @throws IOException If an I/O error occurs.
	 * @see #writeFile(Request, Response)
	 */
	protected Map<String, Object> getPageContext(Request request, Response response) throws IOException
	{
		return defaultPageContext;
	}

	/**
	 * <p>Processes a file specified within the request as a FreeMarker template and writes the result to the response.</p>
	 * <p>The following variables are passed to the template:</p>
	 * <ul>
	 * <li>request - The Request object.</li>
	 * <li>response - The Response object.</li>
	 * <li>session - The Session associated with this request, or null if not present.</li>
	 * <li>context - The ServerContext object.</li>
	 * <li>page - The page context, as returned from this.getPageContext(Request, Response).</li>
	 * </ul>
	 *
	 * @param request The request object holding information about the request, specifically the requested file.
	 * @param response The response object providing functionality to send a response to the client.
	 * @throws IOException If an I/O error occurs when processing the FreeMarker template.
	 * @see #getPageContext(Request, Response)
	 */
	protected void writeFile(Request request, Response response) throws IOException
	{
		try
		{
			Map<String, Object> dataModel = new HashMap<String, Object>();

			dataModel.put("request", request);
			dataModel.put("response", response);	
			dataModel.put("session", request.getSession(false));
			dataModel.put("context", this.getContext());
			dataModel.put("page", this.getPageContext(request, response));

			this.config.getTemplate(request.getFilePath()).process(dataModel, response.getWriter());
		}
		catch(Exception e)
		{
			throw new IOException("Failed to process free marker template", e);
		}
	}
}
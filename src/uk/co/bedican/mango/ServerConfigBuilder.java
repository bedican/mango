/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

import java.net.*;
import java.io.*;
import javax.xml.parsers.*;
import javax.xml.xpath.*;
import org.w3c.dom.*;

/**
 * Used to build the server configuration from a configuration file.
 */
public class ServerConfigBuilder
{
	private XPath xp;
	private DocumentBuilder documentBuilder;

	/**
	 * Creates a new ServerConfigBuilder instance.
	 *
	 * @throws ServerConfigException If a new instance could not be created.
	 */
	public ServerConfigBuilder() throws ServerConfigException
	{
		try
		{
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			documentBuilderFactory.setNamespaceAware(true);
			this.documentBuilder = documentBuilderFactory.newDocumentBuilder();

			this.xp = XPathFactory.newInstance().newXPath();
		}
		catch(Exception e)
		{
			throw new ServerConfigException("Failed to create document builder.", e);
		}
	}

	/**
	 * Builds a ServerConfig object from a given configuration file.
	 *
	 * @param filename The configuration file.
	 * @return A server configuration representing the given configuration file.
	 * @throws ServerConfigException If a problem occured during building.
	 */
	public ServerConfig build(String filename) throws ServerConfigException
	{
		if(filename == null) {
			throw new NullPointerException();
		}
		if(filename.isEmpty()) {
			throw new IllegalArgumentException("Missing filename");
		}

		File file = new File(filename);

		if(!file.isFile()) {
			throw new ServerConfigException(new FileNotFoundException("File " + filename + " does not exist"));
		}

		ServerConfig config = new ServerConfig();

		Document xmldoc;

		try
		{
			xmldoc = this.documentBuilder.parse(file);

			this.setPort(xmldoc, config);
			this.setControllerPort(xmldoc, config);
			this.setWorkers(xmldoc, config);
			this.setPrintStackTrace(xmldoc, config);
			this.setDisplayServerSignature(xmldoc, config);
			this.setDocroot(xmldoc, config);
			this.setRequestLogFile(xmldoc, config);
			this.setErrorLogFile(xmldoc, config);
			this.setSessionManager(xmldoc, config);
			this.setErrorRequestHandlerName(xmldoc, config);
			this.setDefaultRequestHandlerName(xmldoc, config);			
			this.addRequestHandlerConfigs(xmldoc, config);
			this.addDirectoryIndexes(xmldoc, config);
			this.addMimeTypes(xmldoc, config);
			this.addErrorDocuments(xmldoc, config);
			this.setSslEnabled(xmldoc, config);
			this.setSslClientAuth(xmldoc, config);
			this.setSslPort(xmldoc, config);
			this.setKeystore(xmldoc, config);
			this.setKeystorePassword(xmldoc, config);
			this.setKeyPassword(xmldoc, config);
		}
		catch(Exception e)
		{
			throw new ServerConfigException("Failed to parse configuration file: " + e.getMessage(), e);
		}

		return config;
	}

	private void setPort(Document xmldoc, ServerConfig config) throws Exception
	{
		String serverPort = (String)this.xp.compile("/server/config/port/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if(!serverPort.isEmpty()) {
			config.setPort(Integer.parseInt(serverPort));
		}
	}

	private void setControllerPort(Document xmldoc, ServerConfig config) throws Exception
	{
		String controllerPort = (String)this.xp.compile("/server/config/controller-port/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if(!controllerPort.isEmpty()) {
			config.setControllerPort(Integer.parseInt(controllerPort));
		}
	}

	private void setWorkers(Document xmldoc, ServerConfig config) throws Exception
	{
		String serverWorkers = (String)this.xp.compile("/server/config/workers/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if(!serverWorkers.isEmpty()) {
			config.setWorkers(Integer.parseInt(serverWorkers));
		}
	}

	private void setPrintStackTrace(Document xmldoc, ServerConfig config) throws Exception
	{
		String printStackTrace = (String)this.xp.compile("/server/config/print-stack-trace/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if((printStackTrace.equals("true")) || (printStackTrace.equals("yes")) || (printStackTrace.equals("1"))) {
			config.setPrintStackTrace(true);
		} else if((printStackTrace.equals("false")) || (printStackTrace.equals("no")) || (printStackTrace.equals("0"))) {
			config.setPrintStackTrace(false);
		}
	}

	private void setDisplayServerSignature(Document xmldoc, ServerConfig config) throws Exception
	{
		String displayServerSignature = (String)this.xp.compile("/server/config/display-server-signature/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if((displayServerSignature.equals("true")) || (displayServerSignature.equals("yes")) || (displayServerSignature.equals("1"))) {
			config.setDisplayServerSignature(true);
		} else if((displayServerSignature.equals("false")) || (displayServerSignature.equals("no")) || (displayServerSignature.equals("0"))) {
			config.setDisplayServerSignature(false);
		}
	}

	private void setDocroot(Document xmldoc, ServerConfig config) throws Exception
	{
		String serverDocroot = (String)this.xp.compile("/server/config/docroot/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if(!serverDocroot.isEmpty()) {
			config.setDocroot(new File(serverDocroot));
		}
	}

	private void setRequestLogFile(Document xmldoc, ServerConfig config) throws Exception
	{
		String requestLogFilename = (String)this.xp.compile("/server/config/request-log/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if(!requestLogFilename.isEmpty()) {
			config.setRequestLogFile(new File(requestLogFilename));
		}
	}

	private void setErrorLogFile(Document xmldoc, ServerConfig config) throws Exception
	{
		String errorLogFilename = (String)this.xp.compile("/server/config/error-log/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if(!errorLogFilename.isEmpty()) {
			config.setErrorLogFile(new File(errorLogFilename));
		}
	}

	private void setSessionManager(Document xmldoc, ServerConfig config) throws Exception
	{
		String sessionManagerClassName = (String)this.xp.compile("/server/config/session-manager/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if(!sessionManagerClassName.isEmpty()) {
			try { config.setSessionManager(Class.forName(sessionManagerClassName)); } catch(Exception e) {}
		}
	}

	private void setErrorRequestHandlerName(Document xmldoc, ServerConfig config) throws Exception
	{
		String errorHandlerName = (String)this.xp.compile("/server/config/error-handler/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if(!errorHandlerName.isEmpty()) {
			config.setErrorRequestHandlerName(errorHandlerName);
		}
	}

	private void setDefaultRequestHandlerName(Document xmldoc, ServerConfig config) throws Exception
	{
		String defaultHandlerName = (String)this.xp.compile("/server/config/default-handler/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if(!defaultHandlerName.isEmpty()) {
			config.setDefaultRequestHandlerName(defaultHandlerName);
		}
	}

	private void addRequestHandlerConfigs(Document xmldoc, ServerConfig config) throws Exception
	{
		NodeList handlers = (NodeList)this.xp.compile("/server/handlers/handler").evaluate(xmldoc,  XPathConstants.NODESET);

		XPathExpression xpHandlerName = this.xp.compile("name/text()");
		XPathExpression xpHandlerClassName = this.xp.compile("class/text()");
		XPathExpression xpHandlerPattern = this.xp.compile("url-pattern/text()");
		XPathExpression xpHandlerPatterns = this.xp.compile("url-patterns/url-pattern/text()");
		XPathExpression xpHandlerParameters = this.xp.compile("params/param");
		XPathExpression xpHandlerParameterName = this.xp.compile("@name");
		XPathExpression xpHandlerParameterValue = this.xp.compile("text()");

		Class handlerClass;
		String handlerName, handlerClassName, handlerPattern, handlerParameterName, handlerParameterValue;
		NodeList handlerPatterns, handlerParameters;

		RequestHandlerConfig handlerConfig;

		for(int x = 0; x < handlers.getLength(); x++)
		{
			handlerName = (String)xpHandlerName.evaluate(handlers.item(x), XPathConstants.STRING);
			handlerClassName = (String)xpHandlerClassName.evaluate(handlers.item(x), XPathConstants.STRING);
			handlerPattern = (String)xpHandlerPattern.evaluate(handlers.item(x), XPathConstants.STRING);

			if((handlerName.isEmpty()) || (handlerClassName.isEmpty())) {
				continue;
			}

			try {
				handlerClass = Class.forName(handlerClassName);
			} catch(Exception e) {
				continue;
			}

			handlerConfig = new RequestHandlerConfig(handlerName, handlerClass);

			if(!handlerPattern.isEmpty())
			{
				handlerConfig.addPattern(handlerPattern);
			}
			else
			{
				handlerPatterns = (NodeList)xpHandlerPatterns.evaluate(handlers.item(x), XPathConstants.NODESET);
				for(int y = 0; y < handlerPatterns.getLength(); y++)
				{
					handlerPattern = handlerPatterns.item(y).getNodeValue();
					if(!handlerPattern.isEmpty()) {
						handlerConfig.addPattern(handlerPattern);
					}
				}
			}

			handlerParameters = (NodeList)xpHandlerParameters.evaluate(handlers.item(x), XPathConstants.NODESET);
			for(int y = 0; y < handlerParameters.getLength(); y++)
			{
				handlerParameterName = (String)xpHandlerParameterName.evaluate(handlerParameters.item(y), XPathConstants.STRING);
				handlerParameterValue = (String)xpHandlerParameterValue.evaluate(handlerParameters.item(y), XPathConstants.STRING);

				if((handlerParameterName.isEmpty()) || (handlerParameterValue.isEmpty())) {
					continue;
				}

				handlerConfig.setParameter(handlerParameterName, handlerParameterValue);
			}

			config.addRequestHandlerConfig(handlerConfig.getName(), handlerConfig);
		}
	}

	private void addDirectoryIndexes(Document xmldoc, ServerConfig config) throws Exception
	{
		NodeList directoryIndexes = (NodeList)this.xp.compile("/server/directory-index/index/text()").evaluate(xmldoc,  XPathConstants.NODESET);

		String directoryIndex;

		for(int x = 0; x < directoryIndexes.getLength(); x++)
		{
			directoryIndex = directoryIndexes.item(x).getNodeValue();

			if(!directoryIndex.isEmpty()) {
				config.addDirectoryIndex(directoryIndex);
			}
		}
	}

	private void addMimeTypes(Document xmldoc, ServerConfig config) throws Exception
	{
		NodeList mimeTypes = (NodeList)this.xp.compile("/server/mime-types/mime-type").evaluate(xmldoc,  XPathConstants.NODESET);

		XPathExpression xpMimeTypeName = this.xp.compile("type/text()");
		XPathExpression xpMimeTypeExtension = this.xp.compile("extension/text()");
		XPathExpression xpMimeTypeExtensions = this.xp.compile("extensions/extension/text()");

		String mimeTypeName, mimeTypeExtension;
		NodeList mimeTypeExtensions;

		for(int x = 0; x < mimeTypes.getLength(); x++)
		{
			mimeTypeName = (String)xpMimeTypeName.evaluate(mimeTypes.item(x), XPathConstants.STRING);
			mimeTypeExtension = (String)xpMimeTypeExtension.evaluate(mimeTypes.item(x), XPathConstants.STRING);

			if(mimeTypeName.isEmpty()) {
				continue;
			}

			if(!mimeTypeExtension.isEmpty())
			{
				config.addMimeType(mimeTypeExtension, mimeTypeName);
			}
			else
			{
				mimeTypeExtensions = (NodeList)xpMimeTypeExtensions.evaluate(mimeTypes.item(x), XPathConstants.NODESET);
				for(int y = 0; y < mimeTypeExtensions.getLength(); y++)
				{
					mimeTypeExtension = mimeTypeExtensions.item(y).getNodeValue();
					if(!mimeTypeExtension.isEmpty()) {
						config.addMimeType(mimeTypeExtension, mimeTypeName);
					}
				}
			}
		}
	}

	private void addErrorDocuments(Document xmldoc, ServerConfig config) throws Exception
	{
		NodeList errorDocuments = (NodeList)this.xp.compile("/server/error-documents/error-document").evaluate(xmldoc,  XPathConstants.NODESET);

		XPathExpression xpErrorDocumentStatus = this.xp.compile("status/text()");
		XPathExpression xpErrorDocumentLocation = this.xp.compile("location/text()");

		String errorDocumentStatus, errorDocumentLocation;
		Integer errorDocumentStatusInteger;

		for(int x = 0; x < errorDocuments.getLength(); x++)
		{
			errorDocumentStatus = (String)xpErrorDocumentStatus.evaluate(errorDocuments.item(x), XPathConstants.STRING);
			errorDocumentLocation = (String)xpErrorDocumentLocation.evaluate(errorDocuments.item(x), XPathConstants.STRING);

			if((errorDocumentStatus.isEmpty()) || (errorDocumentLocation.isEmpty())) {
				continue;
			}

			try
			{
				config.addErrorDocument(Integer.valueOf(errorDocumentStatus), errorDocumentLocation);
			}
			catch(NumberFormatException nfe)
			{
				// just continue...
			}
		}
	}

	private void setSslEnabled(Document xmldoc, ServerConfig config) throws Exception
	{
		String sslEnabled = (String)this.xp.compile("/server/config/ssl/enabled/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if((sslEnabled.equals("true")) || (sslEnabled.equals("yes")) || (sslEnabled.equals("1"))) {
			config.setSslEnabled(true);
		} else if((sslEnabled.equals("false")) || (sslEnabled.equals("no")) || (sslEnabled.equals("0"))) {
			config.setSslEnabled(false);
		}
	}

	private void setSslClientAuth(Document xmldoc, ServerConfig config) throws Exception
	{
		String sslClientAuth = (String)this.xp.compile("/server/config/ssl/client-auth/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if((sslClientAuth.equals("true")) || (sslClientAuth.equals("yes")) || (sslClientAuth.equals("1"))) {
			config.setSslClientAuth(true);
		} else if((sslClientAuth.equals("false")) || (sslClientAuth.equals("no")) || (sslClientAuth.equals("0"))) {
			config.setSslClientAuth(false);
		}
	}

	private void setSslPort(Document xmldoc, ServerConfig config) throws Exception
	{
		String sslPort = (String)this.xp.compile("/server/config/ssl/port/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if(!sslPort.isEmpty()) {
			config.setSslPort(Integer.parseInt(sslPort));
		}
	}

	private void setKeystore(Document xmldoc, ServerConfig config) throws Exception
	{
		String keystoreFilename = (String)this.xp.compile("/server/config/ssl/keystore/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if(!keystoreFilename.isEmpty()) {
			config.setKeystore(new File(keystoreFilename));
		}
	}

	private void setKeystorePassword(Document xmldoc, ServerConfig config) throws Exception
	{
		String keystorePassword = (String)this.xp.compile("/server/config/ssl/keystore-password/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if(!keystorePassword.isEmpty()) {
			config.setKeystorePassword(keystorePassword);
		}
	}

	private void setKeyPassword(Document xmldoc, ServerConfig config) throws Exception
	{
		String keyPassword = (String)this.xp.compile("/server/config/ssl/key-password/text()").evaluate(xmldoc,  XPathConstants.STRING);

		if(!keyPassword.isEmpty()) {
			config.setKeyPassword(keyPassword);
		}
	}
}
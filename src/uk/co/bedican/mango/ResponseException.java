/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.mango;

/**
 * Signals an error has occured in generating the response.
 */
public class ResponseException extends Exception
{
	/**
	 * Constructs a new ResponseException with the specified cause.
	 *
	 * @param cause The cause.
	 */
	public ResponseException(Throwable cause) { super(cause); }

	/**
	 * Constructs a new ResponseException with the specified detail message and cause.
	 *
	 * @param message The detail message.
	 * @param cause The cause.
	 */
	public ResponseException(String message, Throwable cause) { super(message, cause); }

	/**
	 * Constructs a new ResponseException with the specified detail message.
	 *
	 * @param message The detail message.
	 */
	public ResponseException(String message) { super(message); }
}